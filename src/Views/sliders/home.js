import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Card, FormControl, Alert } from 'react-bootstrap';
import { CircularProgressbar } from 'react-circular-progressbar';
import Icon11 from '../../assets/examples/images/icon/11.png'
import Icon12 from '../../assets/examples/images/icon/12.png'
import Icon13 from '../../assets/examples/images/icon/13.png'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import { getAlerts } from '../../Actions/alert'

class HomeSlider extends Component {

    componentDidMount() {

        const { getAlerts } = this.props
        getAlerts()

    }

    render() {

        const { alerts, populations } = this.props
        let KP_Reached, KP_Tested_for_HIV, KP_Know_their_results  = 0
        populations.map((population) => (population.type == 'KP_Reached') ? KP_Reached = population.value : 0 )
        populations.map((population) => (population.type == 'KP_Tested_for_HIV') ? KP_Tested_for_HIV = population.value : 0 )
        populations.map((population) => (population.type == 'KP_Know_their_results') ? KP_Know_their_results = population.value : 0 )

        
        return (

            <div className="container-fluid ScrollStyle">
                <div className="row info">
                    <div className="col-lg-9">
                        <div className="row">
                            <div className="col-lg-12 bo-b">
                                <div className="title">
                                    <h2>Key Population Prevention to Treatment Cascade</h2>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-sm-12 bo-r">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="icon-img">
                                            <img src={Icon11} alt="" />
                                        </div>
                                    </div>
                                    <div className="col-md-5" >
                                        <CircularProgressbar value={KP_Reached} text={`${KP_Reached}%`} className="donut donut-lg donut-label" />;
                        </div>
                                    <div className="col-lg-12">
                                        <div className="title">
                                            <p>KP Reached</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-sm-12 bo-r">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="icon-img">
                                            <img src={Icon12} alt="" />
                                        </div>
                                    </div>
                                    <div className="col-md-5">
                                        <CircularProgressbar value={KP_Tested_for_HIV} text={`${KP_Tested_for_HIV}%`} className="donut donut-lg donut-label" />;
                        </div>
                                    <div className="col-lg-12">
                                        <div className="title">
                                            <p>KP Tested for HIV</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6 col-lg-4 col-sm-12 Age Wise District Level ">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="icon-img">
                                            <img src={Icon13} alt="" />
                                        </div>
                                    </div>
                                    <div className="col-md-5" >
                                        <CircularProgressbar value={KP_Know_their_results} text={`${KP_Know_their_results}%`} className="donut donut-lg donut-label" />;
                        </div>
                                    <div className="col-lg-12">
                                        <div className="title">
                                            <p>KP Know their results</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="row">
                            <div className="col-lg-12">
                                <Card> 
                                    <Card.Header as="h5">Alerts</Card.Header>
                                    <Card.Body>
                                            {
                                                alerts.map((alert, index) => <Alert variant='success' key={index}>{alert.description}</Alert>)
                                            }
                                        <Button variant="primary">More</Button>
                                    </Card.Body>
                                </Card>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        alerts: state.alerts.alerts,
        populations: state.alerts.populations,

    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ getAlerts }, dispatch)

const enhance = compose(
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
);

export default enhance(HomeSlider)