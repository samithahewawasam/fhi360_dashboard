import React, { Component } from 'react';
import { Container, Form, Button, Col, Nav, Row, Dropdown, Tab } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import { getCoverage, getCoverageSample } from '../../Actions/coverage'

//coverage tabs
import NationalLevel from './coverage-tabs/NationalLevel'
import DistrictLevel from './coverage-tabs/DistrictLevel'
import AgeWiseCoverage from './coverage-tabs/AgeWiseCoverage'
import PartnerLevel from './coverage-tabs/PartnerLevel'
import PeerEducatorLevel from './coverage-tabs/PeerEducatorLevel'
import NewKPRegistration from './coverage-tabs/NewKPRegistration'
import SubTypologyCoverage from './coverage-tabs/SubTypologyCoverage'
import SocialProtectionSchemes from './coverage-tabs/SocialProtectionSchemes'

//utility
import { districts, years, quarters } from './utility'

class coverage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      district: null,
      project: "ALL",
      quarter: "ALL",
      ageWise: false,
      districtLevel: {
        stacked: true
      },
      year: new Date().getFullYear()
    }
  }

  componentDidMount() {

    const { getCoverage, getCoverageSample } = this.props
    getCoverage(this.state)

  }

  onDistrictSelect(district) {

    const { getCoverage, getCoverageSample } = this.props

    this.state.district = district

    if (district == 'ALL') {
      this.setState({
        districtLevel: {
          stacked: true
        }
      })
    } else {
      this.setState({
        districtLevel: {
          stacked: false
        }
      })
    }

    getCoverage(this.state)

  }

  onPojectSelect(project) {
    const { getCoverage, getCoverageSample } = this.props
    this.state.project = project
    getCoverage(this.state)
  }

  onQuarterSelect(quarter) {
    const { getCoverage } = this.props
    this.state.quarter = quarter
    getCoverage(this.state)
  }

  onYearSelect(year) {
    const { getCoverage } = this.props
    this.state.year = year
    getCoverage(this.state)
  }

  onTabSelect(tab) {
    this.state.ageWise = false
    this.state.district = null

    if (tab == 2) {
      this.state.district = 'ALL'
    } 
    else if (tab == 3) {
      this.state.district = 'ALL'
      this.state.ageWise = true
    }
    const { getCoverage } = this.props
    getCoverage(this.state)

  }

  render() {

    const { coverage } = this.props
    const { districtLevel } = this.state

    return (

      <div className="col-xl-12">

        <Tab.Container id="left-tabs-example" defaultActiveKey="1" onSelect={this.onTabSelect.bind(this)}>
          <Row>
            <Col sm={3}>
              <Nav variant="pills" className="flex-column">
                <Nav.Item>
                  <Nav.Link eventKey="1">National Level</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="2">District Level</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="3">Age Wise Coverage</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="4">Partner Level</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="5">Peer Educator Level</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="6">New KP Registration</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="7">Sub-typology Coverage</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="8">Social Protection Schemes</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
            <Col sm={9}>
              <Tab.Content >
                <Tab.Pane eventKey="1">
                  <NationalLevel
                    coverage={coverage}
                    years={years}
                    quarters={quarters}
                    onPojectSelect={this.onPojectSelect.bind(this)}
                    onQuarterSelect={this.onQuarterSelect.bind(this)}
                    onYearSelect={this.onYearSelect.bind(this)}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="2">
                  <DistrictLevel
                    districtLevel={districtLevel}
                    coverage={coverage}
                    years={years}
                    quarters={quarters}
                    districts={districts}
                    onPojectSelect={this.onPojectSelect.bind(this)}
                    onQuarterSelect={this.onQuarterSelect.bind(this)}
                    onYearSelect={this.onYearSelect.bind(this)}
                    onDistrictSelect={this.onDistrictSelect.bind(this)}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="3">
                  <AgeWiseCoverage
                    districtLevel={districtLevel}
                    coverage={coverage}
                    years={years}
                    quarters={quarters}
                    districts={districts}
                    onPojectSelect={this.onPojectSelect.bind(this)}
                    onQuarterSelect={this.onQuarterSelect.bind(this)}
                    onYearSelect={this.onYearSelect.bind(this)}
                    onDistrictSelect={this.onDistrictSelect.bind(this)}
                  />
                </Tab.Pane>
                <Tab.Pane eventKey="4">
                  <PartnerLevel coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
                <Tab.Pane eventKey="5">
                  <PeerEducatorLevel coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
                <Tab.Pane eventKey="6">
                  <NewKPRegistration coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
                <Tab.Pane eventKey="7">
                  <SubTypologyCoverage coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
                <Tab.Pane eventKey="8">
                  <SocialProtectionSchemes coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    coverage: state.coverage
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ getCoverage }, dispatch)

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

export default enhance(coverage)