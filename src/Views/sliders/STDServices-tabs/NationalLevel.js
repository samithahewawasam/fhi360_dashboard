import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Dropdown, FormControl, ListGroup } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';

const NationalLevel = ({ coverage, years, quarters }) => (
  <Container fluid>
    <div className="row">
      <div className="col-xl-12">
        <form>
          <div className="row bo-b">
            <div className="col-lg-7">
              <div className="main-title">
                <h3>Precentage of KP Received STD Services Against Reported Coverage</h3>
              </div>
            </div>
            <div className="col-lg-5">
              <ul className="filters">
                <li>
                  <div className="btn-group open">
                    <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                      Years
                                      </button>
                    <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                      <a className="dropdown-item all-district-select" href="#" role="menuitem">Last 4 Years</a>
                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">2019</a>
                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">2018</a>
                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">2017</a>
                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">2016</a>
                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">2015</a>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="input-group input-group-icon">
                    <div className="btn-group open">
                      <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                        Quarterly
                                        </button>
                      <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                        <a className="dropdown-item" href="#" role="menuitem">Quarter 1</a>
                        <a className="dropdown-item" href="#" role="menuitem">Quarter 2</a>
                        <a className="dropdown-item" href="#" role="menuitem">Quarter 3</a>
                        <a className="dropdown-item" href="#" role="menuitem">Quarter 4</a>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                    <div className="btn-group" role="group">
                      <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                      <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                      <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </form>
      </div>
    </div>
    <div className="row">
      <div className="main-page all-district">
        <div className="col-xl-4 bo-r">
          <div className="heading-baneer">
            <div className="icon-img">
              <img src="assets/examples/images/icon/24.png" alt="" data-default="assets/examples/images/icon/11.png" />
            </div>
            <div className="titele">
              <h2>1,361</h2>
              <h3>MSM : 254</h3>
              <h3>FSW : 125</h3>
              <h3>BB : 500</h3>
              <h3>PWID : 230</h3>
              <h3>TGW : 252</h3>
            </div>
          </div>
        </div>
        <div className="col-xl-8 ">
          <canvas id="STDNLBAR" />
        </div>
      </div>
      <div className="main-page select-district charthide">
        <div className="col-xl-4 bo-r">
          <h1>National Level 2019 STD Services Received by KP</h1>
          <div className="heading-baneer">
            <div className="icon-img">
              <img src="assets/examples/images/icon/26.png" alt="" data-default="assets/examples/images/icon/11.png" />
            </div>
            <div className="titele">
              <h2>1,000</h2>
              <h3>MSM : 154</h3>
              <h3>FSW : 25</h3>
              <h3>BB : 50</h3>
              <h3>PWID : 20</h3>
              <h3>TGW : 22</h3>
            </div>
          </div>
        </div>
        <div className="col-xl-4 ">
          <canvas id="STDNLPIE" />
        </div>
        <div className="col-xl-4">
          <canvas id="STDSUQ" />
        </div>
      </div>
    </div>
  </Container>
)

export default NationalLevel