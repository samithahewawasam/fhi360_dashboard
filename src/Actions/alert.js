export const getAlerts = data => ({ type: 'GET_ALERTS' , data})
export const getAlertsSuccess = (data) => ({ type: "GET_ALERTS_SUCCESS", data })
export const getAlertsFail = error => ({ type: "GET_ALERTS_FAIL", error })