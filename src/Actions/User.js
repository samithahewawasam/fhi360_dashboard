export const signIn = (login) => ({ type: 'SIGN_IN', login})
export const signInSuccess = (data) => ({ type: 'SIGN_IN_SUCCESS', data})
export const signInFail = (error) => ({ type: 'SIGN_IN_FAIL', error})
export const signOut = (data) => ({ type: 'SIGN_OUT', data})
export const signOutSuccess = (data) => ({ type: 'SIGN_OUT_SUCCESS', data})
export const signOutFail = (error) => ({ type: 'SIGN_OUT_FAIL', error})

