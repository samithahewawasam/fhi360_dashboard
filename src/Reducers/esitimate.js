const estimatesData = {
    "Kandy": {
        "total": 23,
        "BB": {
            2019: {
                0: {
                    sum: 5
                },
                1: {
                    sum: 2
                },
                2: {
                    sum: 6
                },
                3: {
                    sum: 8
                },
                4: {
                    sum: 9
                },
                5: {
                    sum: 1
                },
                6: {
                    sum: 3
                },
                7: {
                    sum: 5
                },
                8: {
                    sum: 7
                },
                9: {
                    sum: 9
                },
                10: {
                    sum: 2
                },
                11: {
                    sum: 4
                },
                Q1: 23,
                Q2: 17,
                Q3: 12,
                Q4: 7,
                total: 79
            }
        },
    },
    "Gampaha": {
        "total": 45,
        "FSW": {
            2019: {
                0: {
                    sum: 4
                },
                1: {
                    sum: 8
                },
                2: {
                    sum: 1
                },
                3: {
                    sum: 3
                },
                4: {
                    sum: 2
                },
                5: {
                    sum: 2
                },
                6: {
                    sum: 13
                },
                7: {
                    sum: 2
                },
                8: {
                    sum: 4
                },
                9: {
                    sum: 2
                },
                10: {
                    sum: 9
                },
                11: {
                    sum: 6
                },
                Q1: 28,
                    Q2: 24,
                        Q3: 11,
                            Q4: 3,
                                total: 39
            }
        }
    },
    "Badulla":{
        "total": 34,
        "MSM": {
            2019: {
                0: {
                    sum: 2
                },
                1: {
                    sum: 5
                },
                2: {
                    sum: 7
                },
                3: {
                    sum: 2
                },
                4: {
                    sum: 14
                },
                5: {
                    sum: 8
                },
                6: {
                    sum: 6
                },
                7: {
                    sum: 12
                },
                8: {
                    sum: 2
                },
                9: {
                    sum: 11
                },
                10: {
                    sum: 9
                },
                11: {
                    sum: 3
                },
                Q1: 12,
                    Q2: 23,
                        Q3: 8,
                            Q4: 39,
                                total: 89
            }
        },
    },
    "Batticaloa":{
        "total": 65,
        "PWID": {
            2019: {
                0: {
                    sum: 13
                },
                1: {
                    sum: 3
                },
                2: {
                    sum: 5
                },
                3: {
                    sum: 2
                },
                4: {
                    sum: 8
                },
                5: {
                    sum: 7
                },
                6: {
                    sum: 17
                },
                7: {
                    sum: 2
                },
                8: {
                    sum: 4
                },
                9: {
                    sum: 5
                },
                10: {
                    sum: 8
                },
                11: {
                    sum: 2
                },
                Q1: 12,
                    Q2: 23,
                        Q3: 23,
                            Q4: 9,
                                total: 56
            }
        },
        "TGW": {
            2019: {
                0: {
                    sum: 4
                },
                1: {
                    sum: 8
                },
                2: {
                    sum: 3
                },
                3: {
                    sum: 3
                },
                4: {
                    sum: 9
                },
                5: {
                    sum: 5
                },
                6: {
                    sum: 7
                },
                7: {
                    sum: 3
                },
                8: {
                    sum: 5
                },
                9: {
                    sum: 3
                },
                10: {
                    sum: 9
                },
                11: {
                    sum: 2
                },
                Q1: 21,
                    Q2: 23,
                        Q3: 21,
                            Q4: 32,
                                total: 76
            }
        }
    }
}

export default estimatesData