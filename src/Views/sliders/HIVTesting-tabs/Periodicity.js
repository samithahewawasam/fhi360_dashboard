import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Dropdown, FormControl, ListGroup } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';

const Periodicity = ({ coverage, years, quarters }) => (
    <Container fluid>
        <div className="row">
            <div className="col-xl-12">
                <form>
                    <div className="row bo-b">
                        <div className="col-lg-4">
                            <div className="main-title">
                                <h4>Number of time tested foe HIV duraing last 6 months</h4>
                            </div>
                            <div className="col-lg-8">
                                <ul className="filters">
                                    <li>
                                        <div className="input-group input-group-icon">
                                            <div className="btn-group open">
                                                <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                                    Year
                                        </button>
                                                <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                                    <a className="dropdown-item" href="#" role="menuitem">2017</a>
                                                    <a className="dropdown-item" href="#" role="menuitem">2016</a>
                                                    <a className="dropdown-item" href="#" role="menuitem">2015</a>
                                                    <a className="dropdown-item" href="#" role="menuitem">2014</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="btn-group open">
                                            <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                                KP Typology
                                      </button>
                                            <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                                <a className="dropdown-item all-kp-select" href="#" role="menuitem">All KP Typology</a>
                                                <a className="dropdown-item selected-kp-select" href="#" role="menuitem">MSM</a>
                                                <a className="dropdown-item selected-kp-select" href="#" role="menuitem">FSW</a>
                                                <a className="dropdown-item selected-kp-select" href="#" role="menuitem">BB</a>
                                                <a className="dropdown-item selected-kp-select" href="#" role="menuitem">PWID</a>
                                                <a className="dropdown-item selected-kp-select" href="#" role="menuitem">TGW</a>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="input-group input-group-icon">
                                            <div className="btn-group open">
                                                <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                                    District
                                        </button>
                                                <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                                    <a className="dropdown-item" href="#" role="menuitem">District 01</a>
                                                    <a className="dropdown-item" href="#" role="menuitem">District 02</a>
                                                    <a className="dropdown-item" href="#" role="menuitem">District 03</a>
                                                    <a className="dropdown-item" href="#" role="menuitem">District 04</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="input-group input-group-icon">
                                            <div className="btn-group open">
                                                <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                                    Quarterly
                                        </button>
                                                <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                                    <a className="dropdown-item" href="#" role="menuitem">Quarterly 01</a>
                                                    <a className="dropdown-item" href="#" role="menuitem">Quarterly 02</a>
                                                    <a className="dropdown-item" href="#" role="menuitem">Quarterly 03</a>
                                                    <a className="dropdown-item" href="#" role="menuitem">Quarterly 04</a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                                            <div className="btn-group" role="group">
                                                <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                                                <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                                                <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div className="col-xl-4 bo-r">
                <div className="heading-baneer">
                    <div className="icon-img">
                        <img src="assets/examples/images/icon/27.png" alt="" />
                    </div>
                    <div className="titele">
                        <h4>Annual target for HIV testing</h4>
                        <h4>Total number tested</h4>
                        <h4>Total number tested once duraing last 6 months</h4>
                    </div>
                </div>
                <div className="titele2">
                    <h2>National Level</h2>
                </div>
            </div>
            <div className="col-xl-8 ">
                <canvas id="PEHIVSET" style={{ height: '347px !important', width: '80vw' }} />
            </div>
        </div>
    </Container>
)

export default Periodicity