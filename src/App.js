import React, { Component } from 'react';
import { Provider } from 'react-redux'
import store from './store'
import AppRouter from './Route'

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <AppRouter></AppRouter>
      </Provider>
    );
  }
}

export default App;
