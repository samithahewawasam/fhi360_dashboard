import React, { Component } from 'react';
import { Container, Form, Button, Col, Nav, Row, Dropdown, Tab } from 'react-bootstrap';
import { CircularProgressbar } from 'react-circular-progressbar';

//HIVTesting tabs
import CondomSupplyChain from './commodities-tabs/CondomSupplyChain'
import EducatorLevel from './commodities-tabs/EducatorLevel'

//utility
import { years, quarters } from './utility'

class Commodities extends Component {

  render() {
    const { coverage } = this.props

    return (
      <div className="col-lg-12 col-xl-12">
          <Tab.Container id="left-tabs-example" defaultActiveKey="1">
            <Row>
              <Col sm={3}>
                <Nav variant="pills" className="flex-column">
                  <Nav.Item>
                    <Nav.Link eventKey="1">Condom Supply Chain</Nav.Link>
                  </Nav.Item>
                  <Nav.Item>
                    <Nav.Link eventKey="2">Condom Distribution Peer Educator Level</Nav.Link>
                  </Nav.Item>
                </Nav>
              </Col>
              <Col sm={9}>
                <Tab.Content>
                  <Tab.Pane eventKey="1">
                    <CondomSupplyChain coverage={coverage} years={years} quarters={quarters} />
                  </Tab.Pane>
                  <Tab.Pane eventKey="2">
                    <EducatorLevel coverage={coverage} years={years} quarters={quarters} />
                  </Tab.Pane>
                  </Tab.Content>
                </Col>
              </Row>
            </Tab.Container>
            </div>
        )
    }
}

export default Commodities