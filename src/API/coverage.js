import API from './index';

export const getCoverageData = (requests) => {


    const access_token = localStorage.getItem('access_token');
    const headers = {
            Accept: 'application/json',
            Authorization: `Bearer ${access_token}`
    };

    console.log(requests)
    
    return new Promise((resolve, reject) => {

            API.request({
                method: 'GET',
                url: 'filter',
                params: requests
            }).then(({ data }) => {
                resolve(data)
            }).catch((error) => {
                reject(error)
            })

    })

}
