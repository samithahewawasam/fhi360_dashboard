const initialState = {
    data: []
}

const login = (state = initialState, action) => {
    switch (action.type) {
        case "LOGIN":
            return Object.assign({}, state, {
                data: action.data
            })
        case "LOGIN_SUCCESS":
            return Object.assign({}, state, {
                data: action.data
            })
        case "LOGIN_FAIL":
            return Object.assign({}, state, {
                data: action.data
            })
        default:
            return state
    }
}

export default login;