import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Dropdown, FormControl, ListGroup } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';

const NewKPRegistration = ({ coverage, years, quarters }) => (
    <Container fluid>
  <div className="row">
                      <div className="col-xl-12">
                        <form>
                          <div className="row bo-b">
                            <div className="col-lg-4">
                              <div className="main-title">
                                <h3>New KP Registration Trends </h3>
                              </div>
                            </div>
                            <div className="col-lg-8">
                              <ul className="filters">
                                <li>
                                  <div className="btn-group open">
                                    <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                      Districts
                                      </button>
                                    <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                      <a className="dropdown-item all-district-select" href="#" role="menuitem">All Districts </a>
                                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">Ampara</a>
                                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">Anuradhapura</a>
                                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">Badulla</a>
                                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">Batticaloa</a>
                                      <a className="dropdown-item selected-district-select" href="#" role="menuitem">Colombo</a>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="btn-group open">
                                    <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                      KP Typology
                                      </button>
                                    <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                      <a className="dropdown-item" href="#" role="menuitem">All KP Typologies</a>
                                      <a className="dropdown-item" href="#" role="menuitem">MSM</a>
                                      <a className="dropdown-item" href="#" role="menuitem">FSW</a>
                                      <a className="dropdown-item" href="#" role="menuitem">BB</a>
                                      <a className="dropdown-item" href="#" role="menuitem">PWID</a>
                                      <a className="dropdown-item" href="#" role="menuitem">TGW</a>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="input-group input-group-icon">
                                    <div className="btn-group open">
                                      <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                        Year
                                        </button>
                                      <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                        <a className="dropdown-item" href="#" role="menuitem">2017</a>
                                        <a className="dropdown-item" href="#" role="menuitem">2016</a>
                                        <a className="dropdown-item" href="#" role="menuitem">2015</a>
                                        <a className="dropdown-item" href="#" role="menuitem">2014</a>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="input-group input-group-icon">
                                    <div className="btn-group open">
                                      <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                        Quarterly
                                        </button>
                                      <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                        <a className="dropdown-item" href="#" role="menuitem">Quarter 01</a>
                                        <a className="dropdown-item" href="#" role="menuitem">Quarter 02</a>
                                        <a className="dropdown-item" href="#" role="menuitem">Quarter 03</a>
                                        <a className="dropdown-item" href="#" role="menuitem">Quarter 04</a>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <li>
                                  <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                                    <div className="btn-group" role="group">
                                      <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                                      <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                                      <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>
                    <div className="row">
                      <div className="main-page all-district">
                        <div className="col-xl-4 bo-r">
                          <h1>All Districts Wise Peer Educator Coverage </h1>
                          <div className="heading-baneer">
                            <div className="icon-img">
                              <img src="assets/examples/images/icon/22.png" alt="" data-default="assets/examples/images/icon/22.png" />
                            </div>
                            <div className="titele">
                              <h3>Total new KP registration: </h3>
                              <h2>2,254</h2>
                              <h3>Total KP covered :</h3>
                              <h2>2,561</h2>
                              <h3>Target for KP coverage :</h3>
                              <h2>1,561</h2>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-8 ">
                          <canvas id="NEWRG" />
                        </div>
                      </div>
                      <div className="main-page select-district charthide">
                        <div className="col-xl-4 bo-r">
                          <h1>Anuradhapura Districts Wise Peer Educator Coverage </h1>
                          <div className="heading-baneer">
                            <div className="icon-img">
                              <img src="assets/examples/images/icon/22.png" alt="" data-default="assets/examples/images/icon/22.png" />
                            </div>
                            <div className="titele">
                              <h3>Total new KP registration: </h3>
                              <h2>2,254</h2>
                              <h3>Total KP covered :</h3>
                              <h2>2,561</h2>
                              <h3>Target for KP coverage :</h3>
                              <h2>1,561</h2>
                            </div>
                          </div>
                        </div>
                        <div className="col-xl-8">
                          <canvas id="NEWRG2" />
                        </div>
                      </div>
                    </div>
    </Container>
)

export default NewKPRegistration