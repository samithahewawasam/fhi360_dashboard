import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Dropdown, FormControl, ListGroup } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';

const NationalLevel = ({ coverage, years, quarters, onPojectSelect, onQuarterSelect, onYearSelect }) => (
    <Container fluid>
        <div className="row">
            <div className="col-xl-12">
                <form>
                    <div className="row bo-b">
                        <div className="col-lg-5">
                            <div className="main-title">
                                <h3>National Level Coverage</h3>
                            </div>
                        </div>
                        <div className="col-lg-7">
                            <ul className="filters">
                                <li>
                                    <Dropdown variant='secondary' onSelect={(project) => onPojectSelect(project)} navbar>
                                        <Dropdown.Toggle>
                                            KP Typologies
                                            </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#" eventKey="ALL" active>All KP Typologies</Dropdown.Item>
                                            {
                                                coverage.projects.map((project, index) => <Dropdown.Item href="#" key={index} eventKey={project}>{project}</Dropdown.Item>)
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </li>
                                <li>
                                    <Dropdown variant='secondary' onSelect={(year) => onYearSelect(year)}>
                                        <Dropdown.Toggle>
                                            Year
                                            </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            {
                                                years.map((year, index) => <Dropdown.Item href="#" key={index} eventKey={year}>{year}</Dropdown.Item>)
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </li>
                                <li>
                                    <Dropdown variant='secondary' onSelect={(quarter) => onQuarterSelect(quarter)} eventKey="ALL">
                                        <Dropdown.Toggle>
                                            Quarter
                                            </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#" eventKey="ALL">All Quarters</Dropdown.Item>
                                            {
                                                quarters.map((quarter, index) => <Dropdown.Item href="#" eventKey={`Q${quarter}`} key={index}>Quarter {quarter}</Dropdown.Item>)
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </li>
                                <li>
                                    <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                                        <div className="btn-group" role="group">
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div className="row">
            <div className="main-page all-district">
                <div className="col-xl-4 bo-r">
                    <h1>Coverage Against Estimates</h1>
                    <div className="heading-baneer">
                        <div className="icon-img">
                            <img src="assets/examples/images/icon/14.png" alt="" />
                        </div>
                        <div className="titele">
                            <h3>Estimates :</h3>
                            <h2>5,551</h2>
                            <h3>Target : </h3>
                            <h2>4,914</h2>
                            <h3>Achievement :</h3>
                            <h2>7,551</h2>
                        </div>
                    </div>
                </div>
                <div className="col-xl-8 ">
                    
                    <Bar
                        data={coverage.national}
                        width={100}
                        height={50}
                        options={{
                            maintainAspectRatio: false,
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }}
                    />
                    
                </div>
            </div>
            <div className="main-page select-district charthide">
                <div className="col-xl-4 bo-r">
                    <h1>Coverage of MSM Against Estimates</h1>
                    <div className="heading-baneer">
                        <div className="icon-img">
                            <img src="assets/examples/images/icon/111.png" alt="" />
                        </div>
                        <div className="titele">
                            <h3>Target : </h3>
                            <h2>25</h2>
                            <h3>Achievement :</h3>
                            <h2>25</h2>
                            <h3>Estimates :</h3>
                            <h2>5</h2>
                            <p />
                        </div>
                    </div>
                </div>
                <div className="col-xl-8 ">

                </div>
            </div>
        </div>
    </Container>
)

export default NationalLevel