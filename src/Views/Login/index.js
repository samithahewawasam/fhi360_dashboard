import React, { Component } from 'react';
import { Container, Form, Button } from 'react-bootstrap';
import { Input, Select, Checkbox } from '../../Components/Form'
import { bindActionCreators } from 'redux'
import { Field, reduxForm, change, reset } from 'redux-form'
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import { signIn } from '../../Actions/User'
import { withRouter } from 'react-router-dom'
import './index.css'
import { NotificationContainer, NotificationManager } from 'react-notifications';

//images
import Icon01 from '../../assets/examples/images/icon/01.jpg'
import Icon02 from '../../assets/examples/images/icon/02.jpg'
import Icon03 from '../../assets/examples/images/icon/03.jpg'
import Icon04 from '../../assets/examples/images/icon/04.jpg'
import Icon05 from '../../assets/examples/images/icon/05.jpg'

//css
import '../../assets/global/css/bootstrap.min599c.css?v4.0.2'
import '../../assets/global/css/bootstrap-extend.min599c.css?v4.0.2'
import '../../assets/css/site.min599c.css'
import '../../assets/css/resposive.css'
import '../../assets/global/css/skintools.min599c.css?v4.0.2'
import '../../assets/global/css/skintools.min599c.css?v4.0.2'
import '../../assets/css/custom.css'
import '../../assets/global/vendor/animsition/animsition.min599c.css?v4.0.2'
import '../../assets/global/vendor/asscrollable/asScrollable.min599c.css?v4.0.2'
import '../../assets/global/vendor/switchery/switchery.min599c.css?v4.0.2'
import '../../assets/global/vendor/intro-js/introjs.min599c.css?v4.0.2'
import '../../assets/global/vendor/slidepanel/slidePanel.min599c.css?v4.0.2'
import '../../assets/global/vendor/flag-icon-css/flag-icon.min599c.css?v4.0.2'
import '../../assets/examples/css/pages/login-v2.min599c.css?v4.0.2'
import '../../assets/global/fonts/web-icons/web-icons.min599c.css?v4.0.2'
import '../../assets/global/fonts/brand-icons/brand-icons.min599c.css?v4.0.2'

const validate = values => {
    const errors = {}
    if (!values.password) {
        errors.password = 'This field is required'
    }
    return errors
}

class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            isRedirect: false,
            redirectUrl: null
        }

    }

    handleSubmit = (values) => {

        const { signIn, reset } = this.props;

        signIn(values)

        reset('Login')

    }

    render() {

        const { handleSubmit, pristine, reset, submitting, isPending, profile } = this.props;

        return (

            <Container fluid className="site-navbar-small page-login-v2 layout-full page-dark">
                <div className="page" data-animsition-in="fade-in" data-animsition-out="fade-out">
                    <div className="page-content">
                        <div className="page-brand-info">
                            <div className="brand">
                                <h2 className="brand-text font-size-40">National Key Population</h2>
                                <h2 className="brand-text font-size-40">Dashboard Sri Lanka</h2>
                                <ul>
                                    <li>
                                        <img src={Icon01} alt="" />
                                    </li>
                                    <li>
                                        <img src={Icon02} alt="" />
                                    </li>
                                    <li>
                                        <img src={Icon03} alt="" />
                                    </li>
                                    <li>
                                        <img src={Icon04} alt="" />
                                    </li>
                                    <li>
                                        <img src={Icon05} alt="" />
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="page-login-main animation-slide-right animation-duration-1">
                            <div className="brand hidden-md-up">
                                <h3 className="brand-text font-size-40">Remark</h3>
                            </div>
                            <h3 className="font-size-24">Sign In</h3>
                            <Form>
                                    <Field name="username" component={Input} type="text" placeholder='Email' className="form-control"/>
                                    <Field name="password" component={Input} type="text" placeholder='Password' className="form-control"/>
                                <Form.Group>
                                    <div className="checkbox-custom checkbox-inline checkbox-primary float-left">
                                        <input type="checkbox" id="rememberMe" name="rememberMe" />
                                        <label htmlFor="rememberMe">Remember me</label>
                                    </div>
                                    <a className="float-right" href="index.html">Forgot password?</a>
                                </Form.Group>
                                <Button variant="primary" type="button" onClick={handleSubmit(this.handleSubmit.bind(this))}>Submit</Button>
                            </Form>
                            <p>
                                No account?
              <a href="#">Sign Up</a>
                            </p>
                            <footer className="page-copyright">
                                <p>
                                    Copyright © 2019 Key Population Dashboard.
                <br />All rights reserved
                <br />Designed &amp; Developed by Procons Infotech
              </p>
                                <div className="social">
                                    <a className="btn btn-icon btn-round social-twitter mx-5" href="#">
                                        <i className="icon bd-twitter" aria-hidden="true" />
                                    </a>
                                    <a className="btn btn-icon btn-round social-facebook mx-5" href="#">
                                        <i className="icon bd-facebook" aria-hidden="true" />
                                    </a>
                                    <a className="btn btn-icon btn-round social-google-plus mx-5" href="#">
                                        <i className="icon bd-google-plus" aria-hidden="true" />
                                    </a>
                                </div>
                            </footer>
                        </div>
                    </div>
                </div>
            </Container>
        );
    }
}


const mapStateToProps = (state) => {
    return {
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ signIn, change, reset }, dispatch)

const enhance = compose(
    reduxForm({
        form: 'Login',
        validate
    }),
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withRouter
);

export default enhance(Login)