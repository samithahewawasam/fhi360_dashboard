import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Dropdown, FormControl, ListGroup } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';

const PartnerLevel = ({ coverage, years, quarters }) => (
    <Container fluid>
        <div className="row">
            <div className="col-xl-12">
                <form>
                    <div className="row bo-b">
                        <div className="col-lg-4">
                            <div className="main-title">
                                <h3>District Wise KP Coverage by Partners</h3>
                            </div>
                        </div>
                        <div className="col-lg-8">
                            <ul className="filters">
                                <li>
                                    <div className="btn-group open">
                                        <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                            Select Partner
                                      </button>
                                        <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                            <a className="dropdown-item all-district-select" href="#" role="menuitem">All CSO </a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">CSO 1</a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">CSO 1</a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">CSO 2</a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">CSO 3</a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">CSO 4</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="btn-group open">
                                        <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                            Districts
                                      </button>
                                        <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                            <a className="dropdown-item" href="#" role="menuitem">All Districts</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Ampara</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Anuradhapura</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Badulla</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Batticaloa</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Colombo</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="input-group input-group-icon">
                                        <div className="btn-group open">
                                            <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                                Select Month
                                        </button>
                                            <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                                <a className="dropdown-item" href="#" role="menuitem">January</a>
                                                <a className="dropdown-item" href="#" role="menuitem">February</a>
                                                <a className="dropdown-item" href="#" role="menuitem">March</a>
                                                <a className="dropdown-item" href="#" role="menuitem">April</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                                        <div className="btn-group" role="group">
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div className="row">
            <div className="main-page all-district">
                <div className="col-xl-4 bo-r">
                    <h1>All district wise KP coverage by partner Level</h1>
                    <div className="heading-baneer">
                        <div className="icon-img">
                            <img src="assets/examples/images/icon/21.png" alt="" />
                        </div>
                        <div className="titele">
                            <h3>CSO : </h3>
                            <h2>30,000</h2>
                            <h3>STD Clinic :</h3>
                            <h2>2,500</h2>
                            <h3>Other Partners :</h3>
                            <h2>900</h2>
                        </div>
                    </div>
                </div>
                <div className="col-xl-8">
                    <canvas id="PLBAR" />
                </div>
            </div>
            <div className="main-page select-district charthide">
                <div className="col-xl-4 bo-r">
                    <h1>CSO 1 KP coverage</h1>
                    <div className="heading-baneer">
                        <div className="icon-img">
                            <img src="assets/examples/images/icon/25.png" alt="" />
                        </div>
                        <div className="titele">
                            <h3>CSO : </h3>
                            <h2>30,000</h2>
                            <h3>STD Clinic :</h3>
                            <h2>2,500</h2>
                            <h3>Other Partners :</h3>
                            <h2>900</h2>
                        </div>
                    </div>
                </div>
                <div className="col-xl-8 ">
                    <div className="row">
                        <div className="col-xl-12">
                            <div className="char-name">
                                <p>Anuradhapura District</p>
                            </div>
                            <canvas id="PLPIE" />
                        </div>
                        <div className="col-xl-12">
                            <div className="char-name">
                                <p>Colombo District</p>
                            </div>
                            <canvas id="PLPIE1" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </Container>
)

export default PartnerLevel