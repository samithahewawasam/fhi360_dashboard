const initialState = {
    projects: ["BB", "FSW", "MSM", "PWID", "TGW"],
    national: {},
    summary: {}
}

const coverage = (state = initialState, action) => {
    switch (action.type) {
        case "GET_COVERAGE_SUCCESS":
            return Object.assign({}, state, {
                national: action.data.national,
                summary: action.data.summary || {}
            })
        case "GET_COVERAGE_FAIL":
            return Object.assign({}, state, {
                national: {}
            })
        default:
            return state
    }
}

export default coverage;