import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, NavDropdown, FormControl, Tabs, Tab } from 'react-bootstrap';
//images
import HIVTestingIcon from '../../assets/examples/images/icon/06.png'
import STDservicesIcon from '../../assets/examples/images/icon/05.png'
import CountryScenarioIcon from '../../assets/examples/images/icon/32.png'
import HomeIcon from '../../assets/examples/images/icon/04.png'
import CoverageIcon from '../../assets/examples/images/icon/08.png'
import CommoditiesIcon from '../../assets/examples/images/icon/07.png'
import FinanceIcon from '../../assets/examples/images/icon/09.png'
import HRIcon from '../../assets/examples/images/icon/10.png'
import SwipeableViews from 'react-swipeable-views';
import { bindKeyboard } from 'react-swipeable-views-utils';

import '../../assets/examples/css/charts/chartjs.min599c.css?v4.0.2'
import 'react-circular-progressbar/dist/styles.css';

//sliders
import CountryScenario from '../sliders/country'
import HomeSlider from '../sliders/home'
import Coverage from '../sliders/coverage'
import STDServices from '../sliders/STDServices'
import HIVTesting from '../sliders/HIVTesting'
import Commodities from '../sliders/commodities'
import Finance from '../sliders/Finance'
import HR from '../sliders/HR'

const BindKeyboardSwipeableViews = bindKeyboard(SwipeableViews);

const styles = {
    tabs: {
        background: '#fff',
    },
    slide: {
        padding: 15,
        minHeight: 800,
    }
};

class Slider extends Component {

    state = {
        index: 0,
    };

    handleChange = (eventKey, value) => {

        this.setState({
            index: parseInt(eventKey)
        });
    };

    handleChangeIndex = index => {
        this.setState({
            index,
        });
    };

    render() {

        const { index } = this.state;

        return (

            <Container fluid>

                <Nav onSelect={this.handleChange} fill>
                    <Nav.Item>
                        <Nav.Link href="#" eventKey={0}>
                            <img src={HomeIcon} /> Home
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="#" eventKey={1}>
                            <img src={CountryScenarioIcon} /> Country Scenario
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="#" eventKey={2}>
                            <img src={CoverageIcon} /> Coverage
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="#" eventKey={3}>
                            <img src={STDservicesIcon} /> STD services
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="#" eventKey={4}>
                            <img src={HIVTestingIcon} /> HIV Testing
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="#" eventKey={5}>
                            <img src={CommoditiesIcon} /> Commodities
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="#" eventKey={6}>
                            <img src={FinanceIcon} /> Finance
                        </Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link href="#" eventKey={7}>
                            <img src={HRIcon} /> HR
                        </Nav.Link>
                    </Nav.Item>
                </Nav>

                <BindKeyboardSwipeableViews index={index} onChangeIndex={this.handleChangeIndex} enableMouseEvents>
                    <div style={styles.slide}>
                        <HomeSlider />
                    </div>
                    <div style={styles.slide}>
                        <CountryScenario />
                    </div>
                    <div style={styles.slide}>
                        <Coverage />
                    </div>
                    <div style={styles.slide}>
                        <STDServices />
                    </div>
                    <div style={styles.slide}>
                        <HIVTesting />
                    </div>
                    <div style={styles.slide}>
                        <Commodities />
                    </div>
                    <div style={styles.slide}>
                        <Finance />
                    </div>
                    <div style={styles.slide}>
                        <HR />
                    </div>

                </BindKeyboardSwipeableViews>
            </Container>
        )
    }

}

export default Slider