const districts = ["Ampara", "Anuradhapura", "Badulla", "Batticaloa", "Colombo", "Galle", "Gampaha", "Hambantota", "Jaffna", "Kalutara", "Kandy", "Kegalle", "Kilinochchi", "Kurunegala", "Mannar", "Matale", "Matara", "Moneragala", "Mullaitivu", "Nuwara Eliya", "Polonnaruwa", "Puttalam", "Ratnapura", "Trincomalee", "Vavuniya"];
const coverages = [{
	label: "Estimates",
	backgroundColor: '#FEB85F'
},
{
	label: "Target",
	backgroundColor: '#536EE3'
},
{
	label: "Achievement",
	backgroundColor: '#02A9A1'
}];
const KPTypologies = ["BB", "FSW", "MSM", "PWID", "TGW"];

const getDataSet = ({ project, year, quarter }) => {

    let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let indexQuarter = 0

    switch (quarter) {
        case "1":
            months = ['January', 'February', 'March']
            indexQuarter = 0
            break;
        case "2":
            months = ['April', 'May', 'June']
            indexQuarter = 3
            break;
        case "3":
            months = ['July', 'August', 'September']
            indexQuarter = 6
            break;
        case "4":
            months = ['October', 'November', 'December']
            indexQuarter = 9
            break;
        default:
            break;
    }

    let achievement = []
    let estimates = []

    switch (project) {
        case 'BB':
            achievement = months.map((month, index) => algo.hasOwnProperty("BB") ? algo["BB"][year][index + indexQuarter]["sum"] : 0)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("BB") ? estimatesData["BB"][year][index + indexQuarter]["sum"] : 0)
            break;
        case 'FSW':
            achievement = months.map((month, index) => algo.hasOwnProperty("FSW") ? algo["FSW"][year][index + indexQuarter]["sum"] : 0)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("FSW") ? estimatesData["FSW"][year][index + indexQuarter]["sum"] : 0)
            break;
        case 'MSM':
            achievement = months.map((month, index) => algo.hasOwnProperty("MSM") ? algo["MSM"][year][index + indexQuarter]["sum"] : 0)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("MSM") ? estimatesData["MSM"][year][index + indexQuarter]["sum"] : 0)
            break;
        case 'PWID':
            achievement = months.map((month, index) => algo.hasOwnProperty("PWID") ? algo["PWID"][year][index + indexQuarter]["sum"] : 0)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("PWID") ? estimatesData["PWID"][year][index + indexQuarter]["sum"] : 0)
            break;
        case 'TGW':
            achievement = months.map((month, index) => algo.hasOwnProperty("TGW") ? algo["TGW"][year][index + indexQuarter]["sum"] : 0)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("TGW") ? estimatesData["TGW"][year][index + indexQuarter]["sum"] : 0)
            break;
        default:
            achievement = [
                algo.hasOwnProperty("BB") ? algo["BB"][year]["total"] : 0,
                algo.hasOwnProperty("FSW") ? algo["FSW"][year]["total"] : 0,
                algo.hasOwnProperty("MSM") ? algo["MSM"][year]["total"] : 0,
                algo.hasOwnProperty("PWID") ? algo["PWID"][year]["total"] : 0,
                algo.hasOwnProperty("TGW") ? algo["TGW"][year]["total"] : 0
            ]
            estimates = [
                estimatesData.hasOwnProperty("BB") ? estimatesData["BB"][year]["total"] : 0,
                estimatesData.hasOwnProperty("FSW") ? estimatesData["FSW"][year]["total"] : 0,
                estimatesData.hasOwnProperty("MSM") ? estimatesData["MSM"][year]["total"] : 0,
                estimatesData.hasOwnProperty("PWID") ? estimatesData["PWID"][year]["total"] : 0,
                estimatesData.hasOwnProperty("TGW") ? estimatesData["TGW"][year]["total"] : 0
            ]
            break;
    }

    coverages.map(({ label, backgroundColor }, index) => {
        if (index == 0) {
            dataPoints.push({
                label: label,
                data: estimates,
                backgroundColor: [
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor
                ]
            })
        }
        if (index == 1) {
            dataPoints.push({
                label: label,
                data: Target,
                backgroundColor: [
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor
                ]
            })
        }
        if (index == 2) {
            dataPoints.push({
                label: label,
                data: achievement,
                backgroundColor: [
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor,
                    backgroundColor
                ]
            })
        }
    })

    const chartJsInput = {
        labels: project == 'ALL' ? KPTypologies : months,
        datasets: dataPoints
    }
    dataPoints = []
    return chartJsInput
}

export const CoverageDL = (filters) => ({
    datasets: getDataSet(filters)
})