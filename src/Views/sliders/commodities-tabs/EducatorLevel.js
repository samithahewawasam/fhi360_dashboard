import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Dropdown, FormControl, ListGroup } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';

const EducatorLevel = ({ coverage, years, quarters }) => (
  <Container fluid>
    <div className="row">
      <div className="col-xl-12">
        <form>
          <div className="row bo-b">
            <div className="col-lg-3">
              <div className="main-title">
                <h4>Condom Distribution at Peer Educator Level</h4>
              </div>
            </div>
            <div className="form-group col-md-1">
              <div className="input-group input-group-icon">
                <div className="btn-group open">
                  <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                    District
                                    </button>
                  <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                    <a className="dropdown-item " href="#" data-tag="allpe">All Districts</a>
                    <a className="dropdown-item " href="#" data-tag="onedpe">District 01</a>
                    <a className="dropdown-item " href="#" data-tag="onedpe">District 02</a>
                    <a className="dropdown-item " href="#" data-tag="onedpe">District 03</a>
                    <a className="dropdown-item " href="#" data-tag="onedpe">District 04</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group col-md-2 ">
              <div className="btn-group open">
                <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                  KP Typology
                                  </button>
                <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                  <a className="dropdown-item " href="#" role="menuitem">All KP Typologies</a>
                  <a className="dropdown-item" href="#" role="menuitem">MSM</a>
                  <a className="dropdown-item" href="#" role="menuitem">FSW</a>
                  <a className="dropdown-item" href="#" role="menuitem">BB</a>
                  <a className="dropdown-item" href="#" role="menuitem">PWID</a>
                  <a className="dropdown-item" href="#" role="menuitem">TGW</a>
                </div>
              </div>
            </div>
            <div className="form-group col-md-1">
              <div className="input-group input-group-icon">
                <div className="btn-group open">
                  <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                    Year
                                    </button>
                  <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                    <a className="dropdown-item" href="#" role="menuitem">2017</a>
                    <a className="dropdown-item" href="#" role="menuitem">2016</a>
                    <a className="dropdown-item" href="#" role="menuitem">2015</a>
                    <a className="dropdown-item" href="#" role="menuitem">2014</a>
                  </div>
                </div>
              </div>
            </div>
            <div className="form-group col-md-2 ">
              <div className="btn-group open">
                <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                  Select Months
                                  </button>
                <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                  <a className="dropdown-item all-district-select" href="#" role="menuitem">January </a>
                  <a className="dropdown-item selected-district-select" href="#" role="menuitem">February</a>
                  <a className="dropdown-item selected-district-select" href="#" role="menuitem">March</a>
                </div>
              </div>
            </div>
            <div className="form-group col-md-3">
              <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                <div className="btn-group" role="group">
                  <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                  <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                  <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div className="row main-page" id="allpe">
        <div className="col-xl-4 bo-r">
          <h1>Number of PE in All Districts</h1>
          <div className="small-contents">
            <div className="icon-img">
              <img src="assets/examples/images/icon/27.png" alt="" />
            </div>
            <div>
              <h3>Number of  PE Educators</h3>
              <h2>124,914</h2>
            </div>
          </div>
        </div>
        <div className="col-xl-8">
          <canvas id="ALLDPE" />
        </div>
      </div>
      <div className="row main-page hide" id="onedpe">
        <div className="col-xl-4 bo-r">
          <h1>Districts 1</h1>
          <div className="heading-baneer">
            <div className="icon-img">
              <img src="assets/examples/images/icon/24.png" alt="" />
            </div>
            <div className="titele">
              <h3>Number of PE in District</h3>
              <h2>20</h2>
              <h3>Number of Condoms Distributed</h3>
              <h2>1256</h2>
              <p />
            </div>
          </div>
        </div>
        <div className="col-xl-8">
          <canvas id="SDPECD" />
        </div>
      </div>
    </div>
  </Container>
)

export default EducatorLevel