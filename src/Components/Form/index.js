import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';

export const Input = ({
    input,
    label,
    placeholder,
    className,
    action,
    type,
    validate,
    meta: { touched, error, warning },
    ...rest
}) => (
        <Form.Group>
            <Form.Label>{label}</Form.Label>
            <Form.Control {...input} type={type} placeholder={placeholder}  />
            {touched &&
                ((error && <Form.Label basic color='red' pointing>{error}</Form.Label>) ||
                    (warning && <Form.Label basic color='red'>{warning}</Form.Label>))}
        </Form.Group>
    )


export const Select = ({
    input,
    label,
    className,
    options,
    meta: { touched, error, warning },
    ...rest
}) => (
        <Form.Field className={className}>
            <label>{label}</label>
            <Select options={options} onChange={(param, data) => input.onChange(data.value)} {...rest} />
            {touched &&
                ((error && <span>{error}</span>) ||
                    (warning && <span>{warning}</span>))}
        </Form.Field>
    )



export const Checkbox= ({
    input,
    label,
    name,
    className,
    meta: { touched, error, warning }
}) => (
        <Form.Field className={className}>
            <Checkbox  {...input} label={label} onChange={(param, data) => input.onChange(data.value)} className={className} />
            {touched &&
                ((error && <span>{error}</span>) ||
                    (warning && <span>{warning}</span>))}
        </Form.Field>
    )