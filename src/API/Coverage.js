import DataService from './DataService';
import sample from '../Reducers/sample.json'
import estimatesData from '../Reducers/esitimate.js'
import _ from 'lodash';
import algo from '../Reducers/algo'

const from_date = '2019-01-01'
const type = 'coverageData'
let projects = []
let dataPoints = []

const KPTypologies = ["BB", "FSW", "MSM", "PWID", "TGW"];
const districts = ["Ampara", "Anuradhapura", "Badulla", "Batticaloa", "Colombo", "Galle", "Gampaha", "Hambantota", "Jaffna", "Kalutara", "Kandy", "Kegalle", "Kilinochchi", "Kurunegala", "Mannar", "Matale", "Matara", "Moneragala", "Mullaitivu", "Nuwara Eliya", "Polonnaruwa", "Puttalam", "Ratnapura", "Trincomalee", "Vavuniya"];

const Target = [3, 6, 7, 8, 2, 1, 4, 2, 3, 5, 3, 5, 2, 3, 5, 3, 5, 2, 3, 5, 3, 5, 2, 3, 5, 3, 5]

const coverages = [{
    label: "Estimates",
    backgroundColor: '#FEB85F'
},
{
    label: "Target",
    backgroundColor: '#536EE3'
},
{
    label: "Achievement",
    backgroundColor: '#02A9A1'
}];

const getAchievmentByProject = (project, districtKey, year, indexQuarter, months) => {

    console.log("districtKey", districtKey)
    console.log("project", project)

    if (districtKey == 'ALL' && project == 'ALL') {

        districts.map((district, i) => {

            return months.map((month, index) => {

                if (typeof algo[district] !== 'undefined') {

                    return algo[district].hasOwnProperty(project) ?
                        algo[district]['total'] : 0
                }
            }

            )
        })

    } else if (_.includes(districts, districtKey)) {

        return _.map(algo[districtKey], function (project, key) {

            console.log(algo[districtKey][project][year])

            return []


        })

    }

}

const getDefaultAchievment = () => districts.map((district, i) => (typeof algo[district] !== 'undefined') ? algo[district]["total"] : 0)
const getDefaultEstimates = () => districts.map((district, i) => (typeof estimatesData[district] !== 'undefined') ? estimatesData[district]["total"] : 0)

const getDataSet = ({ district, project, year, quarter }) => {
    console.log(district, project, year, quarter)
    let months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let indexQuarter = 0

    switch (quarter) {
        case "1":
            months = ['January', 'February', 'March']
            indexQuarter = 0
            break;
        case "2":
            months = ['April', 'May', 'June']
            indexQuarter = 3
            break;
        case "3":
            months = ['July', 'August', 'September']
            indexQuarter = 6
            break;
        case "4":
            months = ['October', 'November', 'December']
            indexQuarter = 9
            break;
        default:
            break;
    }

    let achievement = []
    let estimates = []

    switch (project) {
        case 'BB':
            achievement = getAchievmentByProject('BB', district, year, quarter, indexQuarter, months)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("BB") ? estimatesData["BB"][year][index + indexQuarter]["sum"] : 0)
            break;
        case 'FSW':
            achievement = getAchievmentByProject('FSW', district, year, quarter, indexQuarter, months)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("FSW") ? estimatesData["FSW"][year][index + indexQuarter]["sum"] : 0)
            break;
        case 'MSM':
            achievement = getAchievmentByProject('MSM', district, year, quarter, indexQuarter, months)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("MSM") ? estimatesData["MSM"][year][index + indexQuarter]["sum"] : 0)
            break;
        case 'PWID':
            achievement = getAchievmentByProject('PWID', district, year, quarter, indexQuarter, months)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("PWID") ? estimatesData["PWID"][year][index + indexQuarter]["sum"] : 0)
            break;
        case 'TGW':
            achievement = getAchievmentByProject('TGW', district, year, quarter, indexQuarter, months)
            estimates = months.map((month, index) => estimatesData.hasOwnProperty("TGW") ? estimatesData["TGW"][year][index + indexQuarter]["sum"] : 0)
            break;
        case 'ALL':
            achievement = getDefaultAchievment()
            estimates = getDefaultEstimates()
            break;
    }

    switch (district) {
        default:
            achievement = getAchievmentByProject(project, district, year, indexQuarter, months)
            estimates = getDefaultEstimates(district)
            break;
    }


    coverages.map(({ label, backgroundColor }, index) => {
        if (index == 0) {
            let colors = []
            districts.map(() => colors.push(backgroundColor))

            dataPoints.push({
                label: label,
                data: estimates,
                backgroundColor: colors
            })
        }
        if (index == 1) {
            let colors = []
            districts.map(() => colors.push(backgroundColor))

            dataPoints.push({
                label: label,
                data: Target,
                backgroundColor: colors
            })
        }
        if (index == 2) {
            let colors = []
            districts.map(() => colors.push(backgroundColor))
            dataPoints.push({
                label: label,
                data: achievement,
                backgroundColor: colors
            })
        }
    })

    let labels = []

    if (district == 'ALL' && project == 'ALL') {
        labels = districts
    } else if (_.includes(districts, district)) {
        labels = months
    } else if (_.includes(KPTypologies, project)) {
        labels = months
    }

    const chartJsInput = {
        labels,
        datasets: dataPoints
    }
    dataPoints = []
    return chartJsInput
}
export const Coverage = (filters) => ({
    datasets: getDataSet(filters)
})
