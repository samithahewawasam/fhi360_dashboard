const initialState = {
    alerts: [],
    populations: []
}

const alerts = (state = initialState, action) => {
    switch (action.type) {
        case "GET_ALERTS_SUCCESS":
            return Object.assign({}, state, {
                alerts: action.data.alerts.data,
                populations: action.data.populations.data,
            })
        case "GET_ALERTS_FAIL":
            return Object.assign({}, state, {
                alerts: [],
                populations: []
            })
        default:
            return state
    }
}

export default alerts;