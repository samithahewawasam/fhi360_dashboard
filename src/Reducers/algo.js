import sample from './sample.json'
import _ from 'lodash'
/**
 * get month index from string 
 * @param {String} month string value
 */

const getMonthFromString = (mon) => {
    return new Date(Date.parse(mon + " 1, 2000")).getMonth()
}

const districts = ["Ampara", "Anuradhapura", "Badulla", "Batticaloa", "Colombo", "Galle", "Gampaha", "Hambantota", "Jaffna", "Kalutara", "Kandy", "Kegalle", "Kilinochchi", "Kurunegala", "Mannar", "Matale", "Matara", "Moneragala", "Mullaitivu", "Nuwara Eliya", "Polonnaruwa", "Puttalam", "Ratnapura", "Trincomalee", "Vavuniya"];

/**
 * group data by project
 * @param {Array} sample - data sample
 */

const get = () => {
    let output = sample.reduce((result, item) => {

        let projects = item.project.split(" ")

        let project = projects[projects.length - 1]

        if (project == 'BB' || project == 'FSW' || project == 'MSM' || project == 'PWID' || project == 'TGW') {

            let districtKey = result[item.district] = result[item.district] || { 'total': 0 };

            let projectKey = districtKey[project] = districtKey[project] || {};

            let year = projectKey[item.year] = projectKey[item.year] || {};

            const monthIndex = getMonthFromString(item.month)

            let month = {}

            _.forEach([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11], function (index) {

                month = year[index] = year[index] || { "values": [], "sum": 0 };
                if (monthIndex == index) {
                    month.values.push(parseFloat(item.ageBelow25) + parseFloat(item.ageAbove25))
                }

            })

        }
        
        return result;

    }, {});


    /**
     * reverse cumulative value
     */

    _.map(output, (district, i) => {

        _.map(district, function (project, key) {

            _.map(project[2019], function (currentMonth, index) {
                const currentMonthCumulative = currentMonth.values.reduce((a, b) => a + b, 0)

                const nextMonthIndex = parseInt(index) - 1
                let previousMonthCumulative = 0

                if (typeof project[2019][nextMonthIndex] != 'undefined') {
                    previousMonthCumulative = project[2019][nextMonthIndex].values.reduce((a, b) => a + b, 0)
                }

                currentMonth.sum = currentMonthCumulative - previousMonthCumulative

            })

        })

    })

    /**
     * get total for each project
     */


    _.map(output, (district, i) => {

        _.map(district, function (project, key) {

            let monthValue = 0
            let Q1 = 0;
            let Q2 = 0;
            let Q3 = 0;
            let Q4 = 0;

            if (key != 'total') {
                _.map(project[2019], function (month, index) {

                    monthValue += month.sum

                    if (index === 0 || index == 1 || index == 2) {
                        Q1 += month.sum
                    }

                    if (index === 3 || index == 4 || index == 5) {
                        Q2 += month.sum
                    }

                    if (index === 6 || index == 7 || index == 8) {
                        Q3 += month.sum
                    }

                    if (index === 9 || index == 10 || index == 11) {
                        Q4 += month.sum
                    }

                })

                project[2019]['total'] = monthValue
                project[2019]['Q1'] = Q1
                project[2019]['Q2'] = Q2
                project[2019]['Q3'] = Q3
                project[2019]['Q4'] = Q4
            }

            district['total'] += monthValue;

        })


    })
    console.log(output)
    return output
}

export default get()