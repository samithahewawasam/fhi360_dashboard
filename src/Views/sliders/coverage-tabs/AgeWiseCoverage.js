import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Dropdown, FormControl, ListGroup } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';

const AgeWiseCoverage = ({ districtLevel, districts, coverage, years, quarters, onPojectSelect, onQuarterSelect, onYearSelect, onDistrictSelect }) => (
    <Container fluid>
        <div className="row">
            <div className="col-xl-12">
                <form>
                    <div className="row bo-b">
                        <div className="col-lg-4">
                            <div className="main-title">
                                <h3>Age Wise Distribution of District Level Coverage</h3>
                            </div>
                        </div>
                        <div className="col-lg-8">
                            <ul className="filters">
                                <li>
                                    <Dropdown variant='secondary' onSelect={(district) => onDistrictSelect(district)} navbar>
                                        <Dropdown.Toggle>
                                            Districts
                    </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#" eventKey="ALL" active>All Districts</Dropdown.Item>
                                            {
                                                districts.map((district, index) => <Dropdown.Item href="#" key={index} eventKey={district}>{district}</Dropdown.Item>)
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </li>
                                <li>
                                    <Dropdown variant='secondary' onSelect={(project) => onPojectSelect(project)} navbar>
                                        <Dropdown.Toggle>
                                            KP Typologies
                    </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#" eventKey="ALL" active>All KP Typologies</Dropdown.Item>
                                            {
                                                coverage.projects.map((project, index) => <Dropdown.Item href="#" key={index} eventKey={project}>{project}</Dropdown.Item>)
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </li>
                                <li>
                                    <Dropdown variant='secondary' onSelect={(year) => onYearSelect(year)}>
                                        <Dropdown.Toggle>
                                            Year
                                            </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            {
                                                years.map((year, index) => <Dropdown.Item href="#" key={index} eventKey={year}>{year}</Dropdown.Item>)
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </li>
                                <li>
                                    <Dropdown variant='secondary' onSelect={(quarter) => onQuarterSelect(quarter)} eventKey="ALL">
                                        <Dropdown.Toggle>
                                            Quarter
                                            </Dropdown.Toggle>

                                        <Dropdown.Menu>
                                            <Dropdown.Item href="#" eventKey="ALL">All Quarters</Dropdown.Item>
                                            {
                                                quarters.map((quarter, index) => <Dropdown.Item href="#" eventKey={`Q${quarter}`} key={index}>Quarter {quarter}</Dropdown.Item>)
                                            }
                                        </Dropdown.Menu>
                                    </Dropdown>
                                </li>
                                <li>
                                    <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                                        <div className="btn-group" role="group">
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div className="row">
            <div className="main-page all-district">
                <div className="col-xl-4 bo-r">
                    <h1>KP typologies, district level coverage achievements</h1>
                    <div className="heading-baneer">
                        <div className="icon-img">
                            <img src="assets/examples/images/icon/15.png" alt="" />
                        </div>
                        <div className="titele">
                            <h3>Totel: </h3>
                            <h2>5,254</h2>
                            <h3>Age &gt; 25 : </h3>
                            <h2>{coverage.summary.ageBelow25}</h2>
                            <h3>Age &lt; 25:</h3>
                            <h2>{coverage.summary.ageAbove25}</h2>
                        </div>
                    </div>
                </div>
                <div className="col-xl-8 ">
                    <Bar
                        data={coverage.national}
                        width={100}
                        height={50}
                        options={{
                            maintainAspectRatio: false,
                            scales: {
                                xAxes: [{
                                    stacked: districtLevel.stacked
                                }],
                                yAxes: [{
                                    stacked: districtLevel.stacked,
                                    ticks: {
                                        beginAtZero: true
                                    }
                                }]
                            }
                        }}
                    />
                </div>
            </div>
        </div>
    </Container>
)

export default AgeWiseCoverage