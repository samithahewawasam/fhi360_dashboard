
// Country Scenario
var ctx = document.getElementById("CSBAR").getContext('2d');
var data = {
  labels: ["MSM","FSW","BB","PWID","TGW"],
  datasets: [{
    label: "HIV",
    backgroundColor: "#00aaa0",
    data: [3,7,5,4,5]
  },
  {
    label: "Sypilis",
    backgroundColor: "#ffb760",
    data: [5,8,4,2, 8]
  }]
};

var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 20,
    title: {
        display: true,
       padding: 10,
         fontSize: 20,
        text: 'HIV and Syphillistrends among key population reported in IBBS,2019'
    },
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
            legend: {
            display: true,
            position :'bottom',
            labels: {
                fontColor: 'black'
            }
        }
  }
});
//-------------------------------------------------------

// Country Scenario
var ctx = document.getElementById("SSCOBAR").getContext('2d');
var data = {
  labels: ["MSM","FSW","BB","PWID","TGW"],
  datasets: [{
    label: "2018",
    backgroundColor: "#00aaa0",
    data: [30,78,5,41,55]
  }]
};

var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 20,
    title: {
        display: true,
       padding: 10,
         fontSize: 20,
        text: 'Condom Use Patterns Among 2019'
    },
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
            legend: {
            display: true,
            position :'bottom',
            labels: {
                fontColor: 'black'
            }
        }
  }
});
//-------------------------------------------------------
// National Level Coverage Bar Chart
var ctx = document.getElementById("NLCBAR").getContext('2d');
var data = {
  labels: ["MSM","FSW","BB","PWID","TGW"],
  datasets: [
{
    label: "Estimates",
    backgroundColor: "#ffb760",
    data: [5,8,4,2, 8]
  },
  {
    label: "Target",
    backgroundColor: "#546de5",
    data: [7,5,6,5, 2]
  },
  {
    label: "Achievement",
    backgroundColor: "#00aaa0",
    data: [3,7,5,4,5]
  }
  
  ]
};
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 20,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
            legend: {
            display: true,
            position :'bottom',
            labels: {
                fontColor: 'black'
            }
        }
  }
});

// National Level Coverage bar Chart
var ctx = document.getElementById("NLCPIE").getContext('2d');
var data = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  datasets: [
{
    label: "Estimates",
    backgroundColor: "#ffb760",
    data: [5,8,4,2,8,5,5,7,8,9,4,5]
  },
  {
    label: "Target",
    backgroundColor: "#546de5",
    data: [7,5,6,5,2,4,4,6,3,8,4,2,]
  },
  {
    label: "Achievement",
    backgroundColor: "#00aaa0",
    data: [3,7,5,4,5,4,4,8,6,2,1,5]
  }
  
  ]
};
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 20,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
            legend: {
            display: true,
            position :'bottom',
            labels: {
                fontColor: 'black'
            }
        }
  }
});
//-------------------------------------------------------------------------------------
// District Level Coverage Base Bar Chart
var ctx = document.getElementById("DLCBAR").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'Target',
      backgroundColor: "#546de5",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25,25,65],
    },
    {
      label: 'Achievement',
      backgroundColor: "#00aaa0",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14,44,56],
    },
    {
      label: 'Estimates',
      backgroundColor: "#ffb760",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14,25,45],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});

//District Level Pic Chart  
var ctx = document.getElementById("DLCPIE").getContext('2d');
var data = {
  labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
  datasets: [
{
    label: "Estimates",
    backgroundColor: "#ffb760",
    data: [5,8,4,2,8,5,5,7,8,9,4,5]
  },
  {
    label: "Target",
    backgroundColor: "#546de5",
    data: [7,5,6,5,2,4,4,6,3,8,4,2,]
  },
  {
    label: "Achievement",
    backgroundColor: "#00aaa0",
    data: [3,7,5,4,5,4,4,8,6,2,1,5]
  }
  
  ]
};
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 20,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
            legend: {
            display: true,
            position :'bottom',
            labels: {
                fontColor: 'black'
            }
        }
  }
});

// ------------------------------------------------------------------------------------------
// Age Wise District Base Bar Chart
var ctx = document.getElementById("agewisealldistrict").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: ' Age > 25',
      backgroundColor: "#546de5",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25,25,45],
    }, 
    {
      label: 'Age < 25 ',
      backgroundColor: "#00aaa0",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14,57,87,25],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});

// Age Wise District Bar Chart 1
    var ctx = document.getElementById("AWPIE1").getContext('2d');
var data = {
  labels: ["MSM","FSW","BB","PWID","TGW"],
  datasets: [
{
    label: "Age > 25",
    backgroundColor: "#546de5",
    data: [5,8,4,2, 8]
  },
  {
    label: "Age < 25",
    backgroundColor: "#00aaa0",
    data: [7,5,6,5, 2]
  }]
};
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 20,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
            legend: {
            display: true,
            position :'bottom',
            labels: {
                fontColor: 'black'
            }
        }
  }
});


// ------------------------------------------------------------------------------------------
// Partner Level Bar Chart
var ctx = document.getElementById("PLBAR").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'MSM',
      backgroundColor: "#fc5c65",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    }, {
      label: 'FSW',
      backgroundColor: "#36a2eb",
      data: [12, 59, 5, 56, 58,12, 59, 85, 23,54,87,96,25,41,36,55,69,25,14,25,6,58,69,41,58],
    }, {
      label: 'BB',
      backgroundColor: "#cc65fe",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,56,25,36,4,84,55,69,25,45,65,25,45,25,25,48,69],
    }, 
    {
      label: 'PWID',
      backgroundColor: "#ffce56",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,14,58,6,9,36,54,58,2,36,5,48,58,69,58,68,25],
    },
    {
      label: 'TGW',
      backgroundColor: "#11c26d",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});

// Partner Level Pie Chart
    var config = {
      type: 'pie',
      data: {
        datasets: [        {
            fill: true,
            backgroundColor: [
                '#fc5c65',
                '#36a2eb',
                '#cc65fe',
                '#ffce56',
                '#11c26d'],
            data: [5, 95,55,66,44],
            borderWidth: [1,1]
        }],
    labels: [
          'MSM',
          'FSW',
          'BB',
          'PWID',
          'TGW',
        ]
      },
      options: {
        responsive: true,
            legend: {
            display: true,
            position:'bottom',
            labels: {
                fontColor: 'black'
            }
        }
      }
    };

    var ctx5 = document.getElementById('PLPIE').getContext('2d');
    window.myPie = new Chart(ctx5, config);

    // Partner Level sub Pie Chart
    var config = {
      type: 'pie',
      data: {
        datasets: [        {
            fill: true,
            backgroundColor: [
                '#fc5c65',
                '#36a2eb',
                '#cc65fe',
                '#ffce56',
                '#11c26d'],
            data: [5, 95,55,66,44],
            borderWidth: [1,1]
        }],
    labels: [
          'MSM',
          'FSW',
          'BB',
          'PWID',
          'TGW',
        ]
      },
      options: {
        responsive: true,
            legend: {
            display: true,
            position:'bottom',
            labels: {
                fontColor: 'black'
            }
        }
      }
    };

    var ctx5 = document.getElementById('PLPIE1').getContext('2d');
    window.myPie = new Chart(ctx5, config);



// -------------------------------------------
// PE Traget District Base Bar Chart
// var ctx = document.getElementById("NEWRG").getContext('2d');
// var myChart = new Chart(ctx, {
//   type: 'bar',
//    data: {
//     labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
//     datasets: [{
//       label: 'Target for coverage',
//       backgroundColor: "#00aaa0",
//       data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
//     }, 
//     {
//       label: 'New KP registered',
//       backgroundColor: "#ffce56",
//    data: [12, 59, 5, 56, 58,12, 59, 85, 23,54,87,96,25,41,36,55,69,25,14,25,6,58,69,41,58],
//     }],
//   },
// options: {
//     tooltips: {
//       displayColors: true,
//       callbacks:{
//         mode: 'x',
//       },
//     },
//     scales: {
//       xAxes: [{
//         stacked: true,
//         gridLines: {
//           display: false,
//         }
//       }],
//       yAxes: [{
//         stacked: true,
//         ticks: {
//           beginAtZero: true,
//         },
//         type: 'linear',
//       }]
//     },
//     responsive: true,
//     maintainAspectRatio: false,
//     legend: { position: 'bottom' },
//   }
// });


Chart.defaults.groupableBar = Chart.helpers.clone(Chart.defaults.bar);

var helpers = Chart.helpers;
Chart.controllers.groupableBar = Chart.controllers.bar.extend({
  calculateBarX: function (index, datasetIndex) {
    // position the bars based on the stack index
    var stackIndex = this.getMeta().stackIndex;
    return Chart.controllers.bar.prototype.calculateBarX.apply(this, [index, stackIndex]);
  },

  hideOtherStacks: function (datasetIndex) {
    var meta = this.getMeta();
    var stackIndex = meta.stackIndex;

    this.hiddens = [];
    for (var i = 0; i < datasetIndex; i++) {
      var dsMeta = this.chart.getDatasetMeta(i);
      if (dsMeta.stackIndex !== stackIndex) {
        this.hiddens.push(dsMeta.hidden);
        dsMeta.hidden = true;
      }
    }
  },

  unhideOtherStacks: function (datasetIndex) {
    var meta = this.getMeta();
    var stackIndex = meta.stackIndex;

    for (var i = 0; i < datasetIndex; i++) {
      var dsMeta = this.chart.getDatasetMeta(i);
      if (dsMeta.stackIndex !== stackIndex) {
        dsMeta.hidden = this.hiddens.unshift();
      }
    }
  },

  calculateBarY: function (index, datasetIndex) {
    this.hideOtherStacks(datasetIndex);
    var barY = Chart.controllers.bar.prototype.calculateBarY.apply(this, [index, datasetIndex]);
    this.unhideOtherStacks(datasetIndex);
    return barY;
  },

  calculateBarBase: function (datasetIndex, index) {
    this.hideOtherStacks(datasetIndex);
    var barBase = Chart.controllers.bar.prototype.calculateBarBase.apply(this, [datasetIndex, index]);
    this.unhideOtherStacks(datasetIndex);
    return barBase;
  },

  getBarCount: function () {
    var stacks = [];

    // put the stack index in the dataset meta
    Chart.helpers.each(this.chart.data.datasets, function (dataset, datasetIndex) {
      var meta = this.chart.getDatasetMeta(datasetIndex);
      if (meta.bar && this.chart.isDatasetVisible(datasetIndex)) {
        var stackIndex = stacks.indexOf(dataset.stack);
        if (stackIndex === -1) {
          stackIndex = stacks.length;
          stacks.push(dataset.stack);
        }
        meta.stackIndex = stackIndex;
      }
    }, this);

    this.getMeta().stacks = stacks;
    return stacks.length;
  },
});

var data = {
  labels: ["Target for Coverage", "New KP Registered", "Total Covered"],
  datasets: [
    {
      label: "Apples",
      backgroundColor: "rgba(99,255,132,0.2)",
      data: [20, 10, 30],
      stack: 1
    },
    {
      label: "Bananas",
      backgroundColor: "rgba(99,132,255,0.2)",
      data: [40, 50, 20],
      stack: 1
    },
    {
      label: "Cookies",
      backgroundColor: "rgba(255,99,132,0.2)",
      data: [60, 20, 20],
      stack: 1
    },
    {
      label: "Apples",
      backgroundColor: "rgba(99,255,132,0.2)",
      data: [20, 10, 30],
      stack: 2
    },
    {
      label: "Bananas",
      backgroundColor: "rgba(99,132,255,0.2)",
      data: [40, 50, 20],
      stack: 2
    },
    {
      label: "Cookies",
      backgroundColor: "rgba(255,99,132,0.2)",
      data: [60, 20, 20],
      stack: 2
    },
    {
      label: "Apples",
      backgroundColor: "rgba(99,255,132,0.2)",
      data: [20, 10, 30],
      stack: 3
    },
    {
      label: "Bananas",
      backgroundColor: "rgba(99,132,255,0.2)",
      data: [40, 50, 20],
      stack: 3
    },
    {
      label: "Cookies",
      backgroundColor: "rgba(255,99,132,0.2)",
      data: [60, 20, 20],
      stack: 3
    },
  ]
};

var ctx = document.getElementById("NEWRG").getContext("2d");
new Chart(ctx, {
  type: 'groupableBar',
  data: data,
  options: {
    legend: {
      labels: {
        generateLabels: function(chart) {
          return Chart.defaults.global.legend.labels.generateLabels.apply(this, [chart]).filter(function(item, i){
              return i <= 2;
          });
        }
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          max: 160,
        },
        stacked: true,
      }]
    }
  }
});

// -------------------------------------------


// New Reigistraton CSO pic chart
var ctx = document.getElementById("NEWRG2").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Total new KP registration","Total KP covered","Target for KP conerage"],
    datasets: [{
      label: 'MSM',
      backgroundColor: "#fc5c65",
      data: [12, 59, 5, 46],
    },
    {
      label: 'FSW',
      backgroundColor: "#36a2eb",
      data: [0, 0, 5, 86],
    },
    {
      label: 'BB',
      backgroundColor: "#cc65fe",
      data: [12, 89, 5, 56],
    },
    {
      label: 'PWID',
      backgroundColor: "#ffce56",
      data: [2, 2, 5, 16],
    },
    {
      label: 'TGW',
      backgroundColor: "#11c26d",
      data: [12, 19, 5, 66],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------


// PE Traget District Base Bar Chart
var ctx = document.getElementById("PELBAR").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'Target',
      backgroundColor: "#00aaa0",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25,58,44],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// PE CSO pic chart



var ctx = document.getElementById("PELBAR2").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Peer Educator 1","Peer Educator 2","Peer Educator 3","Peer Educator 4","Peer Educator 5","Peer Educator 6","Peer Educator 7","Peer Educator 8","Peer Educator 9","Peer Educator 10"],
    datasets: [{
      label: 'MSM',
      backgroundColor: "#fc5c65",
      data: [12, 59, 5, 46,45,14,25,14,36,25],
    },
    {
      label: 'FSW',
      backgroundColor: "#36a2eb",
      data: [0, 0, 5, 86,25,45,66,58,14,25],
    },
    {
      label: 'BB',
      backgroundColor: "#cc65fe",
      data: [12, 89, 5, 56,55,47,69,57,58,25],
    },
    {
      label: 'PWID',
      backgroundColor: "#ffce56",
      data: [2, 2, 5, 16,45,87,25,36,21,45],
    },
    {
      label: 'TGW',
      backgroundColor: "#11c26d",
      data: [12, 19, 5, 66,54,88,2,4,6,4],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});


var ctx = document.getElementById("PELBAR3").getContext('2d');
var data = {
  labels:  ["Peer Educator 1","Peer Educator 2","Peer Educator 3","Peer Educator 4","Peer Educator 5","Peer Educator 6","Peer Educator 7","Peer Educator 8","Peer Educator 9","Peer Educator 10"],
  datasets: [
{
    label: "Target",
    backgroundColor: "#546de5",
    data: [5,8,4,2, 8,5,8,4,1,5]
  },
  {
    label: "Achievement",
    backgroundColor: "#00aaa0",
    data: [7,5,6,5, 2,8,7,4,6,5,]
  }]
};
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 20,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
            legend: {
            display: true,
            position :'bottom',
            labels: {
                fontColor: 'black'
            }
        }
  }
});


// CSO T PE Base Bar Chart
var ctx = document.getElementById("SUBKP1").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Registered","Converge"],
    datasets: [{
      label: 'Street - Based',
      backgroundColor: "#fc5c65",
      data: [12 ,15],
    },
    {
      label: 'Lodge - Based',
      backgroundColor: "#36a2eb",
      data: [50, 25 ],
    },
    {
      label: 'Spa - Based',
      backgroundColor: "#cc65fe",
      data: [23, 10],
    }, 
    {
      label: 'Home - Based',
      backgroundColor: "#ffce56",
      data: [10, 25],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------
// Social proctection 

var barChartData = {
      labels: ['2016', '2017','2018','2019'],
      datasets: [{
        label: 'Housing ',
        backgroundColor: window.chartColors.red,
        stack: 'Self-employment',
        stack: 'Stack 0',
        data: [5,2,6,8,4,2,8]
      }, {
        label: 'Legal-aid',
        backgroundColor: window.chartColors.blue,
        stack: 'Stack 0',
        data: [5,8,4,6,9,2,4]
      },
      {
        label: 'Financial Assistance',
        backgroundColor: window.chartColors.brown,
        stack: 'Stack 0',
        data: [5,8,4,6,9,2,4]
      },
      {
        label: 'Disability Services',
        backgroundColor: window.chartColors.yellow,
        stack: 'Stack 0',
        data: [5,8,4,6,9,2,4]
      },
      {
        label: 'Enrollment in Natinal Register',
        backgroundColor: window.chartColors.green,
        stack: 'Stack 0',
        data: [5,8,4,6,9,2,4]
      },
      {
        label: 'Samrudhi Schemes',
        backgroundColor: window.chartColors.orange,
        stack: 'Stack 0',
        data: [5,8,4,6,9,2,4]
      },
      {
        label: 'Vocational Training',
        backgroundColor: window.chartColors.black,
        stack: 'Stack 0',
        data: [5,8,4,6,9,2,4]
      },  
      ]
    };
    var ctx = document.getElementById("SPKP").getContext('2d');
                window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    title: {
                        display: true,

                        
                    },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                     legend: { position: 'bottom' },
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }
            });

// -------------------------------------------
// Social Protecton Selected District
var ctx = document.getElementById("SPSD").getContext('2d');
var data = {
  labels: ["2019"],
  datasets: [{
    label: "Housing ",
    backgroundColor: "#fc5c65",
    data: [150]
  }, 
  {
    label: "Self-employment",
    backgroundColor: "#36a2eb",
    data: [450]
  },
  {
    label: "Legal-aid",
    backgroundColor: "#cc65fe",
    data: [150]
  },
  {
    label: "Financial Assistance",
    backgroundColor: "#ffce56",
    data: [120]
  },
  {
    label: "Disability Services",
    backgroundColor: "#11c26d",
    data: [350]
  },
  {
    label: "Enrollment in Natinal Register",
    backgroundColor: "#273c75",
    data: [200]
  },
   {
    label: "Samrudhi Schemes",
    backgroundColor: "#8c7ae6",
    data: [150]
  },
  {
    label: "Vocational Training",
    backgroundColor: "#c23616",
    data: [120]
  },
 ]
};
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 30,
     maintainAspectRatio: false,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
      legend: {
      display: true,
      position :'bottom',
        labels: {
                fontColor: 'black'
            }
        }
  }
});

var ctx = document.getElementById("SPSDQ").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Q1"," Q2"," Q3","Q4"],
    datasets: [{
      label: 'Housing',
      backgroundColor: "#fc5c65",
      data: [12,12,12,45 ],
    },
    {
      label: 'Self-employment',
      backgroundColor: "#36a2eb",
      data: [12,12,12,45 ],
    },
    {
      label: 'Legal-aid',
      backgroundColor: "#cc65fe",
      data: [12,12,12,45 ],
    },
    {
      label: 'Financial Assistance',
      backgroundColor: "#ffce56",
      data: [12,12,12,45 ],
    },
    {
      label: 'Disability Services',
      backgroundColor: "#11c26d",
      data: [12,12,12,45 ],
    },
    {
      label: 'Enrollment in Natinal Register',
      backgroundColor: "#273c75",
      data: [12,12,12,45 ],
    },
    {
      label: 'Samrudhi Schemes',
      backgroundColor: "#8c7ae6",
      data: [12,12,12,45 ],
    },
    {
      label: 'Vocational Training',
      backgroundColor: "#c23616",
      data: [12,12,12,45 ],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});

// Sub Kp BAR
var ctx = document.getElementById("SUBKP2").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Registered"," Coverage"],
    datasets: [{
      label: 'Married',
      backgroundColor: "#fc5c65",
      data: [12 ,15],
    },
    {
      label: 'Home - Based',
      backgroundColor: "#ffce56",
      data: [10, 25],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------
// Sub Kp BAR
var ctx = document.getElementById("SUBKP3").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Registered"," Coverage"],
    datasets: [{
      label: 'Daily-injector',
      backgroundColor: "#cc65fe",
      data: [12 ,15],
    },
    {
      label: 'Non-daily injector',
      backgroundColor: "#36a2eb",
      data: [10, 25],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// CSOT PE pic chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
    
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange
            
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'Target',
          'Achievements'
        
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('csotpepic').getContext('2d');
    window.myPie = new Chart(ctx5, config);





// STD National Level Bar chart
   var config = {
      type: 'bar',
      data: {
        labels: ['2016', '2017', '2018', '2019'],
             datasets: [{
                    label: "MSM",
                    fill: !0,
                    backgroundColor: "#fc5c65",
                    borderColor: Config.colors("red", 100),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fc5c65",
                    pointBackgroundColor: Config.colors("#36a2eb", 300),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("#36a2eb", 300),
                    data: [65, 59, 80,45]
                }, {
                    label: "FSW",
                    fill: !0,
                    backgroundColor: "#36a2eb",
                    borderColor: Config.colors("blue", 100),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#36a2eb",
                    pointBackgroundColor: Config.colors("primary", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("primary", 600),
                    data: [28, 48, 40,14]
                },
                {
                    label: "BB",
                    fill: !0,
                    backgroundColor: "#cc65fe",
                    borderColor: Config.colors("purple", 100),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("purple", 100),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("purple", 600),
                    data: [30, 60, 20,36]
                },
                {
                    label: "PWID",
                    fill: !0,
                    backgroundColor: "#ffce56",
                    borderColor: Config.colors("yellow", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("yellow", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("yellow", 600),
                    data: [50,50,50,47]
                },
                 {
                    label: "TGW",
                    fill: !0,
                    backgroundColor: "#11c26d",
                    borderColor: Config.colors("blue", 100),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("green", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("green", 600),
                    data: [55, 48,48,25]
                }]
      },
      options: {
        responsive: true,
        title: {
          display: true
        },
        scales: {
          yAxes: [{
            ticks: {
              min: 10,
              max: 100
            }
          }]
        }
      }
    };

  var ctx3 = document.getElementById('STDNLBAR').getContext('2d');
  window.myLine = new Chart(ctx3, config);


// SDT National Level pie chart

var ctx = document.getElementById("STDNLPIE").getContext('2d');
var data = {
  labels: ["2019"],
  datasets: [{
    label: "MSM",
    backgroundColor: "#fc5c65",
    data: [150]
  }, 
  {
    label: "FSW",
    backgroundColor: "#36a2eb",
    data: [450]
  },
  {
    label: "BB",
    backgroundColor: "#cc65fe",
    data: [150]
  },
  {
    label: "PWID",
    backgroundColor: "#ffce56",
    data: [120]
  },
  {
    label: "TGW",
    backgroundColor: "#11c26d",
    data: [350]
  },
 ]
};
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 30,
     maintainAspectRatio: false,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
      legend: {
      display: true,
      position :'bottom',
        labels: {
                fontColor: 'black'
            }
        }
  }
});



// STD Quter
var ctx = document.getElementById("STDSUQ").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Q1"," Q2"," Q3","Q4"],
    datasets: [{
      label: 'MSM',
      backgroundColor: "#fc5c65",
      data: [12,12,12,45 ],
    },
    {
      label: 'FWS',
      backgroundColor: "#36a2eb",
      data: [12,12,12,45 ],
    },
    {
      label: 'BB',
      backgroundColor: "#cc65fe",
      data: [12,12,12,45 ],
    },
    {
      label: 'PWID',
      backgroundColor: "#ffce56",
      data: [12,12,12,45 ],
    },
    {
      label: 'TGW',
      backgroundColor: "#11c26d",
      data: [12,12,12,45 ],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// STD  District Base Bar Chart

  var ctx = document.getElementById("STDDLBAR").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'MSM',
      backgroundColor: "#fc5c65",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    }, {
      label: 'FSW',
      backgroundColor: "#36a2eb",
      data: [12, 59, 5, 56, 58,12, 59, 85, 23,54,87,96,25,41,36,55,69,25,14,25,6,58,69,41,58],
    }, {
      label: 'BB',
      backgroundColor: "#cc65fe",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,56,25,36,4,84,55,69,25,45,65,25,45,25,25,48,69],
    }, 
    {
      label: 'PWID',
      backgroundColor: "#ffce56",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,14,58,6,9,36,54,58,2,36,5,48,58,69,58,68,25],
    },
    {
      label: 'TGW',
      backgroundColor: "#11c26d",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
  maintainAspectRatio: false,
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});





// -------------------------------------------

// STD District pic chart
var ctx = document.getElementById("STDDLPIE").getContext('2d');
var data = {
  labels: ["2019"],
  datasets: [{
    label: "MSM",
    backgroundColor: "#fc5c65",
    data: [150]
  }, 
  {
    label: "FSW",
    backgroundColor: "#36a2eb",
    data: [450]
  },
  {
    label: "BB",
    backgroundColor: "#cc65fe",
    data: [150]
  },
  {
    label: "PWID",
    backgroundColor: "#ffce56",
    data: [120]
  },
  {
    label: "TGW",
    backgroundColor: "#11c26d",
    data: [350]
  },
 ]
};
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 30,
     maintainAspectRatio: false,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
      legend: {
      display: true,
      position :'bottom',
        labels: {
                fontColor: 'black'
            }
        }
  }
});

// -------------------------------------------

var ctx = document.getElementById("STDDLQU").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Q1"," Q2"," Q3","Q4"],
    datasets: [{
      label: 'MSM',
      backgroundColor: "#fc5c65",
      data: [12,12,12,45 ],
    },
    {
      label: 'FWS',
      backgroundColor: "#36a2eb",
      data: [12,12,12,45 ],
    },
    {
      label: 'BB',
      backgroundColor: "#cc65fe",
      data: [12,12,12,45 ],
    },
    {
      label: 'PWID',
      backgroundColor: "#ffce56",
      data: [12,12,12,45 ],
    },
    {
      label: 'TGW',
      backgroundColor: "#11c26d",
      data: [12,12,12,45 ],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------


// HIV Test KP Typology Base Line Chart
     var barChartData = {
       labels: ['2016', '2017', '2018', '2019'],
datasets: [{
                label: 'MSM',
                backgroundColor: "#fc5c65",
                stack: 'Stack 0',
                data: [10,25,44,69]
            },
            {
                label: 'FSW',
                backgroundColor:  "#36a2eb",
                stack: 'Stack 0',
                data: [10,25,44,69]
            },{
                label: 'BB',
                backgroundColor:  "#cc65fe",
                stack: 'Stack 0',
                data: [10,25,44,69]
            },
            {
                label: 'TGW',
                backgroundColor: "#ffce56",
                stack: 'Stack 0',
                data: [10,25,44,69]
            }, {
                label: 'PWI',
                backgroundColor:  "#11c26d",
                stack: 'Stack 0',
                data: [10,25,44,69]
            },
            {
                label: 'Reported Testing',
                backgroundColor: "#0fb9b1",
                stack: 'Stack 1',
                data: [15,55,69,55]
            },
             {

                label: 'Who Know Their Results',
                backgroundColor: "#546de5",
                stack: 'Stack 2',
                data: [54,55,87,200 ]
            }
            ],
};
var ctx = document.getElementById("NLHIVTEST").getContext('2d');
window.myBar = new Chart(ctx, {
type: 'bar',
  data: barChartData,
             options: {
                    title: {
                        display: false,
                        text: 'Chart.js Bar Chart - Stacked'
                    },
                     legend: { position: 'bottom' },
                    tooltips: {
                        mode: 'index',
                        intersect: false
                    },
                    responsive: true,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }

});


// National Level Single District 
var ctx = document.getElementById("NLHIVTY1").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["MSM"," FWS"," BB","PWID","TGW"],
    datasets: [{
      label: 'Reported Testing',
      backgroundColor: "#fc5c65",
      data: [12,12,12,45,25 ],
    },
    {
      label: 'HIV Testing Reported',
      backgroundColor: "#ffb760",
      data: [12,12,12,45,44 ],
    },
    {
      label: 'Who Know Their Results',
      backgroundColor: "#00aaa0",
      data: [12,12,12,45,54 ],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});


// STD Quter

var ctx = document.getElementById("NLHIVTY2").getContext('2d');
var data = {
  labels: ["2019"],
  datasets: [{
    label: "Reported Testing",
    backgroundColor: "#fc5c65",
    data: [150]
  },
  {
    label: "HIV Testing Reported",
    backgroundColor: "#ffb760",
    data: [500]
  },

  {
    label: "KP Know Their Results",
    backgroundColor: "#00aaa0",
    data: [450]
  },
 ]
};
var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 30,
     maintainAspectRatio: false,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
      legend: {
      display: true,
      position :'bottom',
        labels: {
                fontColor: 'black'
            }
        }
  }
});


// -------------------------------------------


// -------------------------------------------

// HIV TEsting Age wise
//   var ctx = document.getElementById("AGEHIVSET").getContext('2d');
// var data = {
//   labels: ["MSM","FSW","BB","PWID","TGW"],
//   datasets: [{
//     label: "Age > 25",
//     backgroundColor: "#2e5468",
//     data: [3,7,5,4,5]
//   }, 
//   {
//     label: "Age < 25",
//     backgroundColor: "#45c490",
//     data: [7,5,6,5, 2]
//   }]
// };

// var myBarChart = new Chart(ctx, {
//   type: 'bar',
//   data: data,
//   options: {
//     barValueSpacing: 20,
//     scales: {
//       yAxes: [{
//         ticks: {
//           min: 0,
//         }
//       }]
//     }
//   }
// });



Chart.defaults.groupableBar = Chart.helpers.clone(Chart.defaults.bar);

var helpers = Chart.helpers;
Chart.controllers.groupableBar = Chart.controllers.bar.extend({
  calculateBarX: function (index, datasetIndex) {
    // position the bars based on the stack index
    var stackIndex = this.getMeta().stackIndex;
    return Chart.controllers.bar.prototype.calculateBarX.apply(this, [index, stackIndex]);
  },

  hideOtherStacks: function (datasetIndex) {
    var meta = this.getMeta();
    var stackIndex = meta.stackIndex;

    this.hiddens = [];
    for (var i = 0; i < datasetIndex; i++) {
      var dsMeta = this.chart.getDatasetMeta(i);
      if (dsMeta.stackIndex !== stackIndex) {
        this.hiddens.push(dsMeta.hidden);
        dsMeta.hidden = true;
      }
    }
  },

  unhideOtherStacks: function (datasetIndex) {
    var meta = this.getMeta();
    var stackIndex = meta.stackIndex;

    for (var i = 0; i < datasetIndex; i++) {
      var dsMeta = this.chart.getDatasetMeta(i);
      if (dsMeta.stackIndex !== stackIndex) {
        dsMeta.hidden = this.hiddens.unshift();
      }
    }
  },

  calculateBarY: function (index, datasetIndex) {
    this.hideOtherStacks(datasetIndex);
    var barY = Chart.controllers.bar.prototype.calculateBarY.apply(this, [index, datasetIndex]);
    this.unhideOtherStacks(datasetIndex);
    return barY;
  },

  calculateBarBase: function (datasetIndex, index) {
    this.hideOtherStacks(datasetIndex);
    var barBase = Chart.controllers.bar.prototype.calculateBarBase.apply(this, [datasetIndex, index]);
    this.unhideOtherStacks(datasetIndex);
    return barBase;
  },

  getBarCount: function () {
    var stacks = [];

    // put the stack index in the dataset meta
    Chart.helpers.each(this.chart.data.datasets, function (dataset, datasetIndex) {
      var meta = this.chart.getDatasetMeta(datasetIndex);
      if (meta.bar && this.chart.isDatasetVisible(datasetIndex)) {
        var stackIndex = stacks.indexOf(dataset.stack);
        if (stackIndex === -1) {
          stackIndex = stacks.length;
          stacks.push(dataset.stack);
        }
        meta.stackIndex = stackIndex;
      }
    }, this);

    this.getMeta().stacks = stacks;
    return stacks.length;
  },
});

var data = {
  labels: ["January", "February", "March"],
  datasets: [
    {
      label: "Apples",
      backgroundColor: "rgba(99,255,132,0.2)",
      data: [20, 10, 30],
      stack: 1
    },
    {
      label: "Bananas",
      backgroundColor: "rgba(99,132,255,0.2)",
      data: [40, 50, 20],
      stack: 1
    },
   

    {
      label: "Apples",
      backgroundColor: "rgba(99,255,132,0.2)",
      data: [20, 10, 30],
      stack: 3
    },
    {
      label: "Bananas",
      backgroundColor: "rgba(99,132,255,0.2)",
      data: [40, 50, 20],
      stack: 3
    },
    {
      label: "Cookies",
      backgroundColor: "rgba(255,99,132,0.2)",
      data: [60, 20, 20],
      stack: 3
    },
  ]
};

var ctx = document.getElementById("AGEHIVSET").getContext("2d");
new Chart(ctx, {
  type: 'groupableBar',
  data: data,
  options: {
    legend: {
      labels: {
        generateLabels: function(chart) {
          return Chart.defaults.global.legend.labels.generateLabels.apply(this, [chart]).filter(function(item, i){
              return i <= 2;
          });
        }
      }
    },
    scales: {
      yAxes: [{
        ticks: {
          max: 160,
        },
        stacked: true,
      }]
    }
  }
});


// HIV Test Ru Level Base Bar Chart
var ctx = document.getElementById("PEHIVSET").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'Annual target for HIV testing',
      backgroundColor: "#4285f5",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    },
    {
      label: 'Total number reported HIV testing',
      backgroundColor: "#35a854",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,,48,26,14,8,56,55,58,41,23,25],
    },
    {
      label: 'Those who tested on during six month',
      backgroundColor: "#fbbc04",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    },
   
    {
      label: 'Those who tested more than one during six month',
      backgroundColor: "#ea4335",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
  maintainAspectRatio: false,
    tooltips: {
      displayColors: true,
      responsive :true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});





// -------------------------------------------
// National Level Condoms Received and Distribution
  var ctx = document.getElementById("NLCD").getContext('2d');
var data = {
  labels: ["2015","2016","2017","2018","2019"],
  datasets: [{
    label: "Supply of Condoms",
    backgroundColor: "#00aaa0",
    data: [3,7,5,4,5]
  }, 
  {
    label: "Distributed to KP",
    backgroundColor: "#ffb760",
    data: [5,8,4,2, 8]
  }]
};

var myBarChart = new Chart(ctx, {
  type: 'bar',
  data: data,
  options: {
    barValueSpacing: 20,
    scales: {
      yAxes: [{
        ticks: {
          min: 0,
        }
      }]
    },
            legend: {
            display: true,
            position :'bottom',
            labels: {
                fontColor: 'black'
            }
        }
  }
});


// District Level condoms distribution
var ctx = document.getElementById("DLCDB").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'Supply of Condoms',
      backgroundColor: "#4285f5",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    },   
   
    {
      label: 'Distributed to KP',
      backgroundColor: "#ea4335",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      responsive :true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// National Level Condoms Received and Distribution
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [        {
            fill: true,
            backgroundColor: [
                '#4285f5',
                '#35a854',
                '#fbbc04',
                '#ed4232',
                '#324595'],
            data: [50,95,58,44,66],
            borderWidth: [1,1]
        }],
    labels: [
          'Supply of MSM',
          'Supply of FSW',
          'Supply of BB',
          'Supply of PWID',
          'Supply of TGW',
        ]
      },
      options: {
        responsive: true,
            legend: {
            display: true,
            position:'bottom',
            labels: {
                fontColor: 'black'
            }
        }
      }
    };

    var ctx5 = document.getElementById('SDCDB').getContext('2d');
    window.myPie = new Chart(ctx5, config);
// -------------------------------------------

// District Level condoms distribution
var ctx = document.getElementById("ALLDPE").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'Number of PE',
      backgroundColor: "#4285f5",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      responsive :true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// District Level condoms distribution
var ctx = document.getElementById("SDPECD").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["PE 1","PE 2","PE 3","PE 4","PE 5","PE 6","PE 7"],
    datasets: [{
      label: 'Supply of Condoms',
      backgroundColor: "#4285f5",
      data: [12, 59, 5, 56, 58,12, 59],
    },   
   
    {
      label: 'Distributed to KP',
      backgroundColor: "#ea4335",
      data: [12, 59, 5, 56, 58, 12, 25],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      responsive :true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// District Level condoms distribution
var ctx = document.getElementById("DLBD").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'Approved budget',
      backgroundColor: "#4285f5",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    },
    {
      label: 'Budget utilization',
      backgroundColor: "#ea4335",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }
    ],
  },
options: {
    tooltips: {
      displayColors: true,
      responsive :true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// Humam Resoures
var ctx = document.getElementById("HRBAR").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'Peer educators',
      backgroundColor: "#4285f5",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    },
    {
      label: 'Field Suoervisors',
      backgroundColor: "#ea4335",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }
    ],
  },
options: {
    tooltips: {
      displayColors: true,
      responsive :true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------