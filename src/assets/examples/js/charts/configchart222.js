
// KP Typology Base Line Chart
   var config = {
      type: 'line',
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
             datasets: [{
                    label: "MSM",
                    fill: !0,
                    backgroundColor: "rgba(204, 213, 219, .1)",
                    borderColor: Config.colors("blue-grey", 300),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("blue-grey", 300),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("blue-grey", 300),
                    data: [65, 59, 80, 81, 56, 55, 40]
                }, {
                    label: "FSW",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("primary", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("primary", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("primary", 600),
                    data: [28, 48, 40, 19, 86, 27, 90]
                },
                {
                    label: "BB",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("red", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("red", 400),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("red", 600),
                    data: [30, 60, 20, 10, 50, 27, 90]
                },
                {
                    label: "DU",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("yellow", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("yellow", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("yellow", 600),
                    data: [50, 60, 10, 10, 45, 27, 90]
                },
                 {
                    label: "PLHIV",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("green", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("green", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("green", 600),
                    data: [55, 48, 33, 19, 86, 27, 90]
                }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Sample Charts'
        },
        scales: {
          yAxes: [{
            ticks: {
              min: 10,
              max: 100
            }
          }]
        }
      }
    };

  var ctx3 = document.getElementById('main').getContext('2d');
  window.myLine = new Chart(ctx3, config);


// CSO T PE Base Bar Chart
var ctx = document.getElementById("csotpebar").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["CSO 1","CSO 2","CSO 3"," CSO 4"],
    datasets: [{
      label: 'Target',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56],
    }, 
    {
      label: 'Achievements',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// CSOT PE pic chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
    
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange
            
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'Target',
          'Achievements'
        
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('csotpepic').getContext('2d');
    window.myPie = new Chart(ctx5, config);

// STD Typology Base Line Chart
   var config = {
      type: 'line',
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
             datasets: [{
                    label: "MSM",
                    fill: !0,
                    backgroundColor: "rgba(204, 213, 219, .1)",
                    borderColor: Config.colors("blue-grey", 300),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("blue-grey", 300),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("blue-grey", 300),
                    data: [65, 59, 80, 81, 56, 55, 40]
                }, {
                    label: "FSW",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("primary", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("primary", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("primary", 600),
                    data: [28, 48, 40, 19, 86, 27, 90]
                },
                {
                    label: "BB",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("red", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("red", 400),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("red", 600),
                    data: [30, 60, 20, 10, 50, 27, 90]
                },
                {
                    label: "DU",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("yellow", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("yellow", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("yellow", 600),
                    data: [50, 60, 10, 10, 45, 27, 90]
                },
                 {
                    label: "PLHIV",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("green", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("green", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("green", 600),
                    data: [55, 48, 33, 19, 86, 27, 90]
                }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Sample Charts'
        },
        scales: {
          yAxes: [{
            ticks: {
              min: 10,
              max: 100
            }
          }]
        }
      }
    };

  var ctx3 = document.getElementById('stdkpbline').getContext('2d');
  window.myLine = new Chart(ctx3, config);

  // STD  District Base Bar Chart
var ctx = document.getElementById("stddisrictlevelbar").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'MSM',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    }, {
      label: 'FSW',
      backgroundColor: "#45c490",
      data: [12, 59, 5, 56, 58,12, 59, 85, 23,54,87,96,25,41,36,55,69,25,14,25,6,58,69,41,58],
    }, {
      label: 'BB',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,56,25,36,4,84,55,69,25,45,65,25,45,25,25,48,69],
    }, 
    {
      label: 'DU',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,14,58,6,9,36,54,58,2,36,5,48,58,69,58,68,25],
    },
    {
      label: 'PLHIV',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// STD District pic chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'MSM',
          'FSW',
          'BB',
          'DU',
          'PLHIV'
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('stodistrictpic').getContext('2d');
    window.myPie = new Chart(ctx5, config);


// HIV Test KP Typology Base Line Chart
    var config = {
      type: 'line',
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
             datasets: [{
                    label: "MSM",
                    fill: !0,
                    backgroundColor: "rgba(204, 213, 219, .1)",
                    borderColor: Config.colors("blue-grey", 300),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("blue-grey", 300),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("blue-grey", 300),
                    data: [65, 59, 80, 81, 56, 55, 40]
                }, {
                    label: "FSW",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("primary", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("primary", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("primary", 600),
                    data: [28, 48, 40, 19, 86, 27, 90]
                },
                {
                    label: "BB",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("red", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("red", 400),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("red", 600),
                    data: [30, 60, 20, 10, 50, 27, 90]
                },
                {
                    label: "DU",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("yellow", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("yellow", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("yellow", 600),
                    data: [50, 60, 10, 10, 45, 27, 90]
                },
                 {
                    label: "PLHIV",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("green", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("green", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("green", 600),
                    data: [55, 48, 33, 19, 86, 27, 90]
                }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Sample Charts'
        },
        scales: {
          yAxes: [{
            ticks: {
              min: 10,
              max: 100
            }
          }]
        }
      }
    };

  var ctx3 = document.getElementById('hivtestline').getContext('2d');
  window.myLine = new Chart(ctx3, config);


// HIV KPT pic chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
   
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
     
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'HIV Testing Received',
          'Reported Coverage'
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('bb').getContext('2d');
    window.myPie = new Chart(ctx5, config);

 // HIV Test District Base Bar Chart
var ctx = document.getElementById("hivtestbar").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'Target',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    }, 
    {
      label: 'Achievements',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// HIV District pic chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
   
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
     
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'HIV Testing Received',
          'Reported Coverage'
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('hivtestdisrtictpie').getContext('2d');
    window.myPie = new Chart(ctx5, config);
// -------------------------------------------
// HIV Test Ru Level Base Bar Chart
var ctx = document.getElementById("hivtestrulevelbar").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["RU Level 01 ","RU Level 02","RU Level 03","RU Level 04"],
    datasets: [{
      label: 'HIV Testing Received',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56],
    }, 
    {
      label: 'Know Their Results',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// HIV Test Age Wise Base Line Chart
   var config = {
      type: 'line',
      data: {
        labels: ['20 - 30', '31 - 40', '41 - 50', '51 - 60', '60>'],
             datasets: [{
                    label: "HIV Testing Received",
                    fill: !0,
                    backgroundColor: "rgba(204, 213, 219, .1)",
                    borderColor: Config.colors("red", 300),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("red", 300),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("red", 300),
                    data: [20, 22, 20, 20, 5]
                },
                 {
                    label: "Know Their Results",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("green", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("green", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("green", 600),
                    data: [20, 15, 5, 15, 2]
                }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Sample Charts'
        },
        scales: {
          yAxes: [{
            ticks: {
              min: 1,
              max: 60
            }
          }]
        }
      }
    };

  var ctx3 = document.getElementById('hivtestageline').getContext('2d');
  window.myLine = new Chart(ctx3, config);


  // -------------------------------------------

// HIV Test KP base pic chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
   
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
     
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'HIV Testing Received',
          'Know Their Result'
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('hivtestagepie').getContext('2d');
    window.myPie = new Chart(ctx5, config);

// -------------------------------------------

// HIV Age base  Base Bar Chart
var ctx = document.getElementById("hivtestbaseage").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels:  ['20 - 30', '31 - 40', '41 - 50', '51 - 60', '60>'],
    datasets: [{
      label: 'HIV Testing Received',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56,56],
    }, 
    {
      label: 'Know Their Results',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56,45],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// HIV Test Age Base Pic Chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
   
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
     
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'HIV Testing Received',
          'Know Their Result'
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('hivtestagekap').getContext('2d');
    window.myPie = new Chart(ctx5, config);

// -------------------------------------------


// KP Typology Base Line Chart
   var config = {
      type: 'line',
      data: {
        labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
             datasets: [{
                    label: "MSM",
                    fill: !0,
                    backgroundColor: "rgba(204, 213, 219, .1)",
                    borderColor: Config.colors("blue-grey", 300),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("blue-grey", 300),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("blue-grey", 300),
                    data: [65, 59, 80, 81, 56, 55, 40]
                }, {
                    label: "FSW",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("primary", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("primary", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("primary", 600),
                    data: [28, 48, 40, 19, 86, 27, 90]
                },
                {
                    label: "BB",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("red", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("red", 400),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("red", 600),
                    data: [30, 60, 20, 10, 50, 27, 90]
                },
                {
                    label: "DU",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("yellow", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("yellow", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("yellow", 600),
                    data: [50, 60, 10, 10, 45, 27, 90]
                },
                 {
                    label: "PLHIV",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("green", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("green", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("green", 600),
                    data: [55, 48, 33, 19, 86, 27, 90]
                }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Sample Charts'
        },
        scales: {
          yAxes: [{
            ticks: {
              min: 10,
              max: 100
            }
          }]
        }
      }
    };

  var ctx3 = document.getElementById('kpbaselinechart').getContext('2d');
  window.myLine = new Chart(ctx3, config);

//-------------------------------------------------------------------------------------
// KP Typology District Base Bar Chart
var ctx = document.getElementById("kpdistricbasebarchart").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'MSM',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    }, {
      label: 'FSW',
      backgroundColor: "#45c490",
      data: [12, 59, 5, 56, 58,12, 59, 85, 23,54,87,96,25,41,36,55,69,25,14,25,6,58,69,41,58],
    }, {
      label: 'BB',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,56,25,36,4,84,55,69,25,45,65,25,45,25,25,48,69],
    }, 
    {
      label: 'DU',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,14,58,6,9,36,54,58,2,36,5,48,58,69,58,68,25],
    },
    {
      label: 'PLHIV',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------
// Select District Level Pic Chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'Red',
          'Orange',
          'Yellow',
          'Green',
          'Blue'
        ]
      },
      options: {
        responsive: true
      }
    };

    var ctx5 = document.getElementById('kpdistricbasepicchart').getContext('2d');
    window.myPie = new Chart(ctx5, config);


// KP Typology District Base Bar Chart
var ctx2 = document.getElementById("agewisebasebarchart").getContext('2d');
var myChart = new Chart(ctx2, {
  type: 'bar',
  data: {
    labels: ["20-30","31-40","41=50","51-60","61>"],
    datasets: [{
      label: 'MSM',
      backgroundColor: "#caf270",
      data: [12,12,55,11,23],
    }, {
      label: 'FSW',
      backgroundColor: "#45c490",
      data: [12,12,56,14,25],
    }, {
      label: 'BB',
      backgroundColor: "#008d93",
      data: [12,25,66,45,25],
    }, 
    {
      label: 'DU',
      backgroundColor: "#008d93",
      data: [12,45,65,25,36],
    },
    {
      label: 'PLHIV',
      backgroundColor: "#2e5468",
      data: [12,44,66,52,36],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------
// Select age range with kpt Pic Chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'MSM',
          'FSW',
          'BB',
          'DU',
          'PLHIV'
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('agerangewithkpt').getContext('2d');
    window.myPie = new Chart(ctx5, config);

// Age Wise District Base Bar Chart
var ctx = document.getElementById("agewisealldistrict").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: '20-30',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    }, {
      label: '31-40',
      backgroundColor: "#45c490",
      data: [12, 59, 5, 56, 58,12, 59, 85, 23,54,87,96,25,41,36,55,69,25,14,25,6,58,69,41,58],
    }, {
      label: '41-50',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,56,25,36,4,84,55,69,25,45,65,25,45,25,25,48,69],
    }, 
    {
      label: '51-60',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,14,58,6,9,36,54,58,2,36,5,48,58,69,58,68,25],
    },
    {
      label: '61>',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// Select age range with kpt Pic Chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'MSM',
          'FSW',
          'BB',
          'DU',
          'PLHIV'
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('agewisedistrict').getContext('2d');
    window.myPie = new Chart(ctx5, config);


    // CSO Base Bar Chart
var ctx = document.getElementById("csobased").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
  data: {
    labels: ["CSO 1","CSO 2","CSO 4","CSO 4","CSO 5","CSO 6","CSO 7","CSO 8"],
    datasets: [{
      label: '20-30',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56, 58,12, 59, 87],
    }, {
      label: '31-40',
      backgroundColor: "#45c490",
      data: [12, 59, 5, 56, 58,12, 59, 85],
    }, {
      label: '41-50',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65],
    }, 
    {
      label: '51-60',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65],
    },
    {
      label: '61>',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56, 58, 12, 59, 12, ],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});

// CSO Pic Chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'MSM',
          'FSW',
          'BB',
          'DU',
          'PLHIV'
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('scopic').getContext('2d');
    window.myPie = new Chart(ctx5, config);

  // CSO District Base Bar Chart
var ctx = document.getElementById("csobaseddisrict").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: '20-30',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    }, {
      label: '31-40',
      backgroundColor: "#45c490",
      data: [12, 59, 5, 56, 58,12, 59, 85, 23,54,87,96,25,41,36,55,69,25,14,25,6,58,69,41,58],
    }, {
      label: '41-50',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,56,25,36,4,84,55,69,25,45,65,25,45,25,25,48,69],
    }, 
    {
      label: '51-60',
      backgroundColor: "#008d93",
      data: [12, 59, 5, 56, 58,12, 59, 65, 51,14,58,6,9,36,54,58,2,36,5,48,58,69,58,68,25],
    },
    {
      label: '61>',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// Select Disrict CSO
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
            randomScalingFactor(),
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange,
            window.chartColors.yellow,
            window.chartColors.green,
            window.chartColors.blue,
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'MSM',
          'FSW',
          'BB',
          'DU',
          'PLHIV'
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('districtcos').getContext('2d');
    window.myPie = new Chart(ctx5, config);


      // PE Traget District Base Bar Chart
var ctx = document.getElementById("petdistrict").getContext('2d');
var myChart = new Chart(ctx, {
  type: 'bar',
   data: {
    labels: ["Ampara","Anuradhapura","Badulla","Batticaloa","Colombo","Eastern","Galle","Gampaha","Hambantota","Jaffna","Kalutara","Kandy","Kegalle","Kilinochchi","Kurunegala","Mannar","Matale","Matara","Monaragala","Mullaitivu","Nuwara Eliya","Polonnaruwa","Puttalam","Puttalam","Ratnapura","Trincomalee","Vavuniya"],
    datasets: [{
      label: 'Target',
      backgroundColor: "#caf270",
      data: [12, 59, 5, 56, 58,12, 59, 87, 45,15,55,48,65,25,45,48,26,14,8,56,55,58,41,23,25],
    }, 
    {
      label: 'Achievements',
      backgroundColor: "#2e5468",
      data: [12, 59, 5, 56, 58, 12, 59, 12, 74,47,88,55,25,6,4,58,9,5,25,65,47,58,69,25,14],
    }],
  },
options: {
    tooltips: {
      displayColors: true,
      callbacks:{
        mode: 'x',
      },
    },
    scales: {
      xAxes: [{
        stacked: true,
        gridLines: {
          display: false,
        }
      }],
      yAxes: [{
        stacked: true,
        ticks: {
          beginAtZero: true,
        },
        type: 'linear',
      }]
    },
    responsive: true,
    maintainAspectRatio: false,
    legend: { position: 'bottom' },
  }
});
// -------------------------------------------

// PE CSO pic chart
var randomScalingFactor = function() {
      return Math.round(Math.random() * 100);
    };
    var config = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            randomScalingFactor(),
            randomScalingFactor(),
    
          ],
          backgroundColor: [
            window.chartColors.red,
            window.chartColors.orange
            
          ],
          label: 'Dataset 1'
        }],
        labels: [
          'Target',
          'Achievements'
        
        ]
      },
      options: {
        responsive: true
      }
    };
    var ctx5 = document.getElementById('petsco').getContext('2d');
    window.myPie = new Chart(ctx5, config);

