import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, NavDropdown, FormControl } from 'react-bootstrap';
import { CircularProgressbar } from 'react-circular-progressbar';
import Icon11 from '../../assets/examples/images/icon/11.png'
import Icon12 from '../../assets/examples/images/icon/12.png'
import Icon13 from '../../assets/examples/images/icon/13.png'

class Finance extends Component {

    render() {
        return (

            <div className="col-lg-12 col-xl-12">
              {/* Example Tabs Left */}
              <div className="example-wrap">
                <div className="col-xl-12">
                  <form>
                    <div className="row bo-b">
                      <div className="col-lg-5">
                        <div className="main-title">
                          <h4>District level utilization of financial resources </h4>
                        </div>
                      </div>
                      <div className="form-group col-md-1 ">
                        <div className="btn-group open">
                          <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                            Districts
                          </button>
                          <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                            <a className="dropdown-item " href="#" data-tag="hrbar1">All Districts </a>
                            <a className="dropdown-item " href="#" data-tag="hrbar2">Anuradhapura</a>
                            <a className="dropdown-item " href="#" data-tag="hrbar2">Ampara</a>
                            <a className="dropdown-item " href="#" data-tag="hrbar2">Badulla</a>
                            <a className="dropdown-item " href="#" data-tag="hrbar2">Colombo</a>
                            <a className="dropdown-item " href="#" data-tag="hrbar2">Gampha</a>
                          </div>
                        </div>
                      </div>
                      <div className="form-group col-md-1">
                        <div className="input-group input-group-icon">
                          <div className="btn-group open">
                            <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                              Year
                            </button>
                            <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                              <a className="dropdown-item" href="#" role="menuitem">2017</a>
                              <a className="dropdown-item" href="#" role="menuitem">2016</a>
                              <a className="dropdown-item" href="#" role="menuitem">2015</a>
                              <a className="dropdown-item" href="#" role="menuitem">2014</a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group col-md-1">
                        <div className="input-group input-group-icon">
                          <div className="btn-group open">
                            <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                              Quarter
                            </button>
                            <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                              <a className="dropdown-item" href="#" role="menuitem">Quarter 01</a>
                              <a className="dropdown-item" href="#" role="menuitem">Quarter 02</a>
                              <a className="dropdown-item" href="#" role="menuitem">Quarter 03</a>
                              <a className="dropdown-item" href="#" role="menuitem">Quarter 04</a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="form-group col-md-1 ">
                        <div className="btn-group open">
                          <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                            Partner/STD clinic
                          </button>
                          <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                            <a className="dropdown-item selected-kp-select" href="#" role="menuitem">Partner</a>
                            <a className="dropdown-item selected-kp-select" href="#" role="menuitem">STD clinic</a>
                          </div>
                        </div>
                      </div>
                      <div className="form-group col-md-3">
                        <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                          <div className="btn-group" role="group">
                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div className="row main-page" id="hrbar1">
                  <div className="col-xl-4 bo-r">
                    <div className="small-contents">
                      <div className="icon-img">
                        <img src="assets/examples/images/icon/27.png" alt="" />
                      </div>
                      <div>
                        <h3>Total budget approved for CSOs</h3>
                        <h2>7,551</h2>
                        <h3>Total utilization by CSOs during last quarter</h3>
                        <h2>7,551</h2>
                        <h3>Total utilization by STD clinics during last quarter</h3>
                        <h2>7,551</h2>
                      </div>
                    </div>
                  </div>
                  <div className="col-xl-8">
                    <canvas id="DLBD" />
                  </div>
                </div>
              </div>
              {/* End Example Tabs Left */}
            </div>
        )
    }
}

export default Finance