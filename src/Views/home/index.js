import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, NavDropdown, FormControl } from 'react-bootstrap';
import { Field, reduxForm, change, reset, getFormValues } from 'redux-form'
import { connect } from 'react-redux'
import { bindActionCreators, compose } from 'redux'
import { withRouter } from 'react-router-dom'
import jQuery from 'jquery';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import { signOut } from '../../Actions/User'
import Slider from './slider'

//css
import '../../assets/global/css/bootstrap.min599c.css?v4.0.2'
import '../../assets/global/css/bootstrap-extend.min599c.css?v4.0.2'
import '../../assets/css/site.min599c.css'
import '../../assets/css/resposive.css'
import '../../assets/global/css/skintools.min599c.css?v4.0.2'
import '../../assets/css/custom.css'
import logo from '../../assets/examples/images/icon/logo.png'

class Home extends Component {

    constructor(props) {
        super(props)
        this.state = {
            activeIndex: 0
        }

        this.handleTabChange = this.handleTabChange.bind(this)
    }

    handleTabChange = (e, { activeIndex }) => this.setState({ activeIndex })


    render() {

        return (
            <Container fluid className="dashboard container">
                <Navbar expand="lg" fixed="top" className="site-navbar navbar navbar-default navbar-fixed-top navbar-mega navbar-inverse">
                    <Navbar.Brand href="#">National Key Population Dashboard</Navbar.Brand>
                </Navbar>
                <Slider />
                <footer className="site-footer" style={{ position: "relative", bottom: 0, left: 0, right: 0, height: 50 }}>
                    <div className="site-footer-legal">Copyright © 2019 <a href="index.html">National Key Population Dashboard</a>. All rights reserved</div>
                    <div className="site-footer-right">
                        <ul>
                            <li><img src={logo} alt="" /></li>
                            <li>Designed & Developed by <a href="http://proconsinfotech.com/">Procons Infotech</a></li>
                        </ul>
                    </div>
                </footer>
            </Container>
        )
    }

}

const mapStateToProps = (state) => {
    return {

    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ signOut }, dispatch)

const enhance = compose(
    reduxForm({
        form: 'Home'
    }),
    connect(
        mapStateToProps,
        mapDispatchToProps
    ),
    withRouter
);

export default enhance(Home)