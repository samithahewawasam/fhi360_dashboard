/*!
 * Remark (http://getbootstrapadmin.com/remark)
 * Copyright 2017 amazingsurge
 * Licensed under the Themeforest Standard Licenses
 */

 $(document).ready(function ($) {
aaaaaaa();
   });

 $(document).ready(function ($) {
aga_wise_chart();
   });
  $(document).ready(function ($) {
distril_level_kp();
   });

function aaaaaaa (){
    var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['17-25', '26-30', '31-45', '56>',],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5],
            backgroundColor: [
                '#f59b2d',
                '#17a79d',
                '#dc4c27',
                '#102f43'
               
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
                
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
};


function distril_level_kp(){
var dlkp = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['17-25', '26-30', '31-45', '56>',],
        datasets: [{
            label: '# of Votes',
            data: [12, 19, 3, 5],
            backgroundColor: [
                '#f59b2d',
                '#17a79d',
                '#dc4c27',
                '#102f43'
               
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)'
                
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
 window.onload = function() {
           var ctx = document.getElementById('dlkp');
            window.dlkp = new Chart(ctx, config);
        };
            window.dlkp.update();


};

function aga_wise_chart(){
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                    ],
                    backgroundColor: [
                        window.chartColors.red,
                        window.chartColors.orange,
                        window.chartColors.yellow,
                        window.chartColors.green,
                        window.chartColors.blue,
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    'MSM',
                    'FSW',
                    'DU',
                    'BB',
                    'PLHIV'
                ]

            },
            options: {
                responsive: true,
                legend: {
         display: true,
    position: 'right',
    fullWidth:true

      }

            }
        };

        window.onload = function() {
            var ctx = document.getElementById('chart-area').getContext('2d');
            window.myPie = new Chart(ctx, config);
        };
            window.myPie.update();
  
};



function newchart(){
     var ctx = document.getElementById('chart-area');
    var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var config = {
            type: 'pie',
            data: {
                datasets: [{
                    data: [
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                        randomScalingFactor(),
                    ],
                    backgroundColor: [
                        window.chartColors.red,
                        window.chartColors.orange,
                        window.chartColors.yellow,
                        window.chartColors.green,
                        window.chartColors.blue,
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    'Red',
                    'Orange',
                    'Yellow',
                    'Green',
                    'Blue'
                ]
            },
            options: {
                responsive: true
            }
        };

        window.onload = function() {
            var ctx = document.getElementById('chart-area').getContext('2d');
            window.myPie = new Chart(ctx, config);
        };

        document.getElementById('randomizeData').addEventListener('click', function() {
            config.data.datasets.forEach(function(dataset) {
                dataset.data = dataset.data.map(function() {
                    return randomScalingFactor();
                });
            });

            window.myPie.update();
        });

        var colorNames = Object.keys(window.chartColors);
        document.getElementById('addDataset').addEventListener('click', function() {
            var newDataset = {
                backgroundColor: [],
                data: [],
                label: 'New dataset ' + config.data.datasets.length,
            };

            for (var index = 0; index < config.data.labels.length; ++index) {
                newDataset.data.push(randomScalingFactor());

                var colorName = colorNames[index % colorNames.length];
                var newColor = window.chartColors[colorName];
                newDataset.backgroundColor.push(newColor);
            }

            config.data.datasets.push(newDataset);
            window.myPie.update();
        });

        document.getElementById('removeDataset').addEventListener('click', function() {
            config.data.datasets.splice(0, 1);
            window.myPie.update();
        });
};





! function(global, factory) {
    if ("function" == typeof define && define.amd) define("/charts/chartjs", ["jquery", "Site"], factory);
    else if ("undefined" != typeof exports) factory(require("jquery"), require("Site"));
    else {
        var mod = {
            exports: {}
        };
        factory(global.jQuery, global.Site), global.chartsChartjs = mod.exports
    }
}(this, function(_jquery, _Site) {
    "use strict";
    var _jquery2 = babelHelpers.interopRequireDefault(_jquery),
        Site = babelHelpers.interopRequireWildcard(_Site);
    (0, _jquery2.default)(document).ready(function($) {
            Site.run()
        }), Chart.defaults.global.responsive = !0,

        function() {
            var lineChartData = {
                labels: ["January", "February", "March", "April", "May", "June", "July"],
                datasets: [{
                    label: "MSM",
                    fill: !0,
                    backgroundColor: "rgba(204, 213, 219, .1)",
                    borderColor: Config.colors("blue-grey", 300),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("blue-grey", 300),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("blue-grey", 300),
                    data: [65, 59, 80, 81, 56, 55, 40]
                }, {
                    label: "FSW",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("primary", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("primary", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("primary", 600),
                    data: [28, 48, 40, 19, 86, 27, 90]
                },
                {
                    label: "BB",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("red", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("red", 400),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("red", 600),
                    data: [30, 60, 20, 10, 50, 27, 90]
                },
                {
                    label: "DU",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("yellow", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("yellow", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("yellow", 600),
                    data: [50, 60, 10, 10, 45, 27, 90]
                },
                 {
                    label: "FSW",
                    fill: !0,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: Config.colors("green", 600),
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: Config.colors("green", 600),
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("green", 600),
                    data: [55, 48, 33, 19, 86, 27, 90]
                }]

            };


            new Chart(document.getElementById("exampleChartjsLine").getContext("2d"), {
                type: "line",
                data: lineChartData,
                options: {
                    responsive: !0,
                    scales: {
                        xAxes: [{
                            display: !0
                        }],
                        yAxes: [{
                            display: !0
                        }]
                    }
                }
            })
        }(),



        function() {
            var barChartData = {
                labels: ["MSM","FSW","BB","DU","PLHIV"],
                datasets: [{
                    label: "17-25",
                    backgroundColor: "rgba(204, 213, 219, .2)",
                    borderColor: Config.colors("blue-grey", 300),
                    hoverBackgroundColor: "rgba(204, 213, 219, .3)",
                    borderWidth: 2,
                    data: [45,25,45,45,58]
                }, 
                {
                    label: "26-30",
                    backgroundColor: "rgba(204, 213, 219, .2)",
                    borderColor: Config.colors("red", 300),
                    hoverBackgroundColor: "rgba(204, 213, 219, .3)",
                    borderWidth: 2,
                    data: [45,35,85,15,58]
                },
                {
                    label: "31-45",
                    backgroundColor: "rgba(204, 213, 219, .2)",
                    borderColor: Config.colors("yellow", 300),
                    hoverBackgroundColor: "rgba(204, 213, 219, .3)",
                    borderWidth: 2,
                    data: [54,25,85,25,54]
                },
                {
                    label: "45-60",
                    backgroundColor: "rgba(204, 213, 219, .2)",
                    borderColor: Config.colors("green", 300),
                    hoverBackgroundColor: "rgba(204, 213, 219, .3)",
                    borderWidth: 2,
                    data: [53,85,35,25,64]
                },

                {
                    label: "61>",
                    backgroundColor: "rgba(98, 168, 234, .2)",
                    borderColor: Config.colors("primary", 600),
                    hoverBackgroundColor: "rgba(98, 168, 234, .3)",
                    borderWidth: 2,
                    data: [94,74,50,29,54]
                }]
            };
            new Chart(document.getElementById("exampleChartjsBar").getContext("2d"), {
                type: "bar",
                data: barChartData,
                options: {
                    responsive: !0,
                    scales: {
                        xAxes: [{
                            display: !0
                        }],
                        yAxes: [{
                            display: !0
                        }]
                    }
                }
            })
        }(),

        
        function() {
            var radarChartData = {
                labels: ["Eating", "Drinking", "Sleeping", "Designing", "Coding", "Partying", "Running"],
                pointLabelFontSize: 14,
                datasets: [{
                    label: "First",
                    pointRadius: 4,
                    borderDashOffset: 2,
                    backgroundColor: "rgba(98, 168, 234, .15)",
                    borderColor: "rgba(0,0,0,0)",
                    pointBackgroundColor: Config.colors("primary", 600),
                    pointBorderColor: "#fff",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("primary", 600),
                    data: [65, 59, 90, 81, 56, 55, 40]
                }, {
                    label: "Second",
                    pointRadius: 4,
                    borderDashOffset: 2,
                    backgroundColor: "rgba(250,122,122,0.25)",
                    borderColor: "rgba(0,0,0,0)",
                    pointBackgroundColor: Config.colors("red", 500),
                    pointBorderColor: "#fff",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: Config.colors("red", 500),
                    data: [28, 48, 40, 19, 96, 27, 100]
                }]
            };
            new Chart(document.getElementById("exampleChartjsRadar").getContext("2d"), {
                type: "radar",
                data: radarChartData,
                options: {
                    responsive: !0,
                    scale: {
                        ticks: {
                            beginAtZero: !0
                        }
                    }
                }
            })
        }(),
        function() {
            var chartData = {
                datasets: [{
                    data: [300, 200, 150, 100],
                    backgroundColor: [Config.colors("red", 400), Config.colors("green", 400), Config.colors("yellow", 400), Config.colors("blue", 400)],
                    label: "My dataset"
                }],
                labels: ["Red", "Green", "Yellow", "Blue"]
            };
            new Chart(document.getElementById("exampleChartjsPloarArea").getContext("2d"), {
                data: chartData,
                type: "polarArea",
                options: {
                    responsive: !0,
                    elements: {
                        arc: {
                            borderColor: "#ffffff"
                        }
                    }
                }
            })
        }(),
        function() {
            var pieData = {
                labels: ["Red", "Blue", "Yellow"],
                datasets: [{
                    data: [300, 50, 100],
                    backgroundColor: [Config.colors("red", 400), Config.colors("green", 400), Config.colors("yellow", 400)],
                    hoverBackgroundColor: [Config.colors("red", 600), Config.colors("green", 600), Config.colors("yellow", 600)]
                }]
            };
            new Chart(document.getElementById("exampleChartjsPie").getContext("2d"), {
                type: "pie",
                data: pieData,
                options: {
                    responsive: !0
                }
            })
        }(),
        function() {
            var doughnutData = {
                labels: ["Red", "Blue", "Yellow"],
                datasets: [{
                    data: [300, 50, 100],
                    backgroundColor: [Config.colors("red", 400), Config.colors("green", 400), Config.colors("yellow", 400)],
                    hoverBackgroundColor: [Config.colors("red", 600), Config.colors("green", 600), Config.colors("yellow", 600)]
                }]
            };
            new Chart(document.getElementById("exampleChartjsDonut").getContext("2d"), {
                type: "doughnut",
                data: doughnutData,
                options: {
                    responsive: !0
                }
            })
        }()
});

