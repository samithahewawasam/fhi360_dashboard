import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, NavDropdown, FormControl } from 'react-bootstrap';
import { CircularProgressbar } from 'react-circular-progressbar';
import Icon11 from '../../assets/examples/images/icon/11.png'
import Icon12 from '../../assets/examples/images/icon/12.png'
import Icon13 from '../../assets/examples/images/icon/13.png'

class HR extends Component {

    render() {
        return (

            <div className="col-lg-12 col-xl-12">
              {/* Example Tabs Left */}
              <div className="example-wrap">
                <div className="row">
                  <div className="col-xl-12">
                    <form>
                      <div className="row bo-b">
                        <div className="col-lg-5">
                          <div className="main-title">
                            <h4>Status of Human Resources</h4>
                          </div>
                        </div>
                        <div className="form-group col-md-1 ">
                          <div className="btn-group open">
                            <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                              District
                            </button>
                            <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                              <a className="dropdown-item " href="#" data-tag="dlbd">All Districts </a>
                              <a className="dropdown-item " href="#" data-tag="dlbd">Anuradhapura</a>
                              <a className="dropdown-item " href="#" data-tag="dlbd">Ampara</a>
                              <a className="dropdown-item " href="#" data-tag="dlbd">Badulla</a>
                              <a className="dropdown-item " href="#" data-tag="dlbd">Colombo</a>
                              <a className="dropdown-item " href="#" data-tag="dlbd">Gampha</a>
                            </div>
                          </div>
                        </div>
                        <div className="form-group col-md-1">
                          <div className="btn-group open">
                            <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                              KP typology
                            </button>
                            <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                              <a className="dropdown-item all-district-select" href="#" role="menuitem">All KP Typologies</a>
                              <a className="dropdown-item selected-district-select" href="#" role="menuitem">MSM</a>
                              <a className="dropdown-item selected-district-select" href="#" role="menuitem">FSW</a>
                              <a className="dropdown-item selected-district-select" href="#" role="menuitem">BB</a>
                              <a className="dropdown-item selected-district-select" href="#" role="menuitem">PWID</a>
                              <a className="dropdown-item selected-district-select" href="#" role="menuitem">TGW</a>
                            </div>
                          </div>
                        </div>
                        <div className="form-group col-md-1">
                          <div className="input-group input-group-icon">
                            <div className="btn-group open">
                              <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                Year
                              </button>
                              <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                                <a className="dropdown-item" href="#" role="menuitem">2017</a>
                                <a className="dropdown-item" href="#" role="menuitem">2016</a>
                                <a className="dropdown-item" href="#" role="menuitem">2015</a>
                                <a className="dropdown-item" href="#" role="menuitem">2014</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-group col-md-1">
                          <div className="input-group input-group-icon">
                            <div className="btn-group open">
                              <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                Quarter
                              </button>
                              <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                                <a className="dropdown-item" href="#" role="menuitem">Quarter 01</a>
                                <a className="dropdown-item" href="#" role="menuitem">Quarter 02</a>
                                <a className="dropdown-item" href="#" role="menuitem">Quarter 03</a>
                                <a className="dropdown-item" href="#" role="menuitem">Quarter 04</a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-group col-md-1 ">
                          <div className="btn-group open">
                            <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                              Partner
                            </button>
                            <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform'}}>
                              <a className="dropdown-item selected-kp-select" href="#" role="menuitem">Partner</a>
                              <a className="dropdown-item selected-kp-select" href="#" role="menuitem">STD clinic</a>
                            </div>
                          </div>
                        </div>
                        <div className="form-group col-md-2">
                          <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                            <div className="btn-group" role="group">
                              <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                              <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                              <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                  <div className="row main-page" id="dlbd">
                    <div className="col-xl-4 bo-r">
                      <div className="small-contents">
                        <div className="titele">
                          <h3>National level allocation of peer educators</h3>
                          <h2>5,551</h2>
                          <h3>Total number of peer educators available during last quarter</h3>
                          <h2>751</h2>
                          <h3>National level allocation of field supervisors</h3>
                          <h2>56,551</h2>
                          <h3>Total number of field supervisors available during last quarter </h3>
                          <h2>75,551</h2>
                        </div>
                      </div>
                    </div>
                    <div className="col-xl-8">
                      <canvas id="HRBAR" />
                    </div>
                  </div>
                  <div className="row main-page hide" id="alldhivtest">
                    <div className="col-xl-5 bo-r">
                      <h1>All Districts</h1>
                      <div className="heading-baneer">
                        <div className="icon-img">
                          <img src="assets/examples/images/icon/24.png" alt="" />
                        </div>
                        <div className="titele">
                          <h3>National Level Supply of Condoms</h3>
                          <h2>256</h2>
                          <h3>National Level Condoms Distributed to KP</h3>
                          <h2>256</h2>
                          <p />
                        </div>
                      </div>
                      <div className="titele2">
                        <p>Number of KP received HIV testing against reported coverage</p>
                      </div>
                    </div>
                    <div className="col-xl-7">
                      <canvas id="DLCDB" />
                    </div>
                  </div>
                  <div className="row main-page hide" id="sdcdb">
                    <div className="col-xl-5 bo-r">
                      <h1>All Districts</h1>
                      <div className="heading-baneer">
                        <div className="icon-img">
                          <img src="assets/examples/images/icon/26.png" alt="" />
                        </div>
                        <div className="titele">
                          <h3>National Level Supply of Condoms</h3>
                          <h2>256</h2>
                          <h3>National Level Condoms Distributed to KP</h3>
                          <h2>256</h2>
                          <p />
                        </div>
                      </div>
                      <div className="titele2">
                        <p>Number of KP received HIV testing against reported coverage</p>
                      </div>
                    </div>
                    <div className="col-xl-7">
                      <canvas id="SDCDB" />
                    </div>
                  </div>
                </div>
              </div>
              {/* End Example Tabs Left */}
            </div>
        )
    }
}

export default HR