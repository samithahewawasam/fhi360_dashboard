import API from './index';

export const getAlerts = () => {

    const access_token = localStorage.getItem('access_token');
    const headers = {
            Accept: 'application/json',
            Authorization: `Bearer ${access_token}`
    };

    const alerts =  new Promise((resolve, reject) => {

            API.request({
                method: 'GET',
                url: 'http://ec2-3-227-194-181.compute-1.amazonaws.com/v1/alerts',
                headers: headers
            }).then(({ data }) => {
                resolve(data)
            }).catch((error) => {
                reject(error)
            })

    })

    const populations =  new Promise((resolve, reject) => {

        API.request({
            method: 'GET',
            url: 'http://ec2-3-227-194-181.compute-1.amazonaws.com/v1/populations',
            headers: headers
        }).then(({ data }) => {
            resolve(data)
        }).catch((error) => {
            reject(error)
        })

})

    return Promise.all([alerts, populations]).then(function(values) {
        return {
            alerts: values[0],
            populations: values[1]
        }
    });

}
