import React, { Component } from 'react';
import { Container, Form, Button, Col, Nav, Row, Dropdown, Tab } from 'react-bootstrap';
import { CircularProgressbar } from 'react-circular-progressbar';

//HIVTesting tabs
import NationalLevel from './HIVTesting-tabs/NationalLevel'
import AgeDistribution from './HIVTesting-tabs/AgeDistribution'
import Periodicity from './HIVTesting-tabs/Periodicity'

//utility
import { years, quarters } from './utility'

class HIVTesting extends Component {

  render() {

    const { coverage } = this.props

    return (

      <div className="col-xl-12">
        <Tab.Container id="left-tabs-example" defaultActiveKey="1">
          <Row>
            <Col sm={3}>
              <Nav variant="pills" className="flex-column">
                <Nav.Item>
                  <Nav.Link eventKey="1">HIV Testing - National Level</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="2">HIV Testing - Age Distribution</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="3">HIV Testing - Periodicity</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
            <Col sm={9}>
              <Tab.Content>
                <Tab.Pane eventKey="1">
                  <NationalLevel coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
                <Tab.Pane eventKey="2">
                  <AgeDistribution coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
                <Tab.Pane eventKey="3">
                  <Periodicity coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    )
  }
}

export default HIVTesting