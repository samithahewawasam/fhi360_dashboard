import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Dropdown, FormControl, ListGroup } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';

const SubTypologyCoverage = ({ coverage, years, quarters }) => (
    <Container fluid>
        <div className="row">
            <div className="col-xl-12">
                <form>
                    <div className="row bo-b">
                        <div className="col-lg-5">
                            <div className="main-title">
                                <h3>District Level Sub-Typology Coverage</h3>
                            </div>
                        </div>
                        <div className="col-lg-7">
                            <ul className="filters">
                                <li>
                                    <div className="btn-group open">
                                        <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                            Districts
                                      </button>
                                        <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                            <a className="dropdown-item" href="#" role="menuitem">All Districts</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Ampara</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Anuradhapura</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Badulla</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Batticaloa</a>
                                            <a className="dropdown-item" href="#" role="menuitem">Colombo</a>
                                        </div>
                                    </div></li>
                                <li>
                                    <div className="input-group input-group-icon">
                                        <div className="btn-group open">
                                            <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown1" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                                Quarterly
                                        </button>
                                            <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                                <a className="dropdown-item" href="#" role="menuitem">Quarter 01</a>
                                                <a className="dropdown-item" href="#" role="menuitem">Quarter 02</a>
                                                <a className="dropdown-item" href="#" role="menuitem">Quarter 03</a>
                                                <a className="dropdown-item" href="#" role="menuitem">Quarter 04</a>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                                        <div className="btn-group" role="group">
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div className="row ">
            <div className="main-page">
                {/* <div class="col-xl-3 bo-r">
                                                          <div class="heading-baneer">
                                                              <div class="icon-img">
                                                                  <img src="assets/examples/images/icon/23.png" alt="">
                                                              </div>
                                                              <div class="titele">
                                                                  <h3>Target : </h3>
                                                                  <h2>1,254</h2>
                                                                  <h3>Achievement :</h3>
                                                                  <h2>2,561</h2>
                                                              </div>
                                                          </div>
                                                      </div> */}
                <div className="col-xl-4 bo-r">
                    <p className="char-name">FSW</p>
                    <canvas id="SUBKP1" />
                </div>
                <div className="col-xl-4 bo-r">
                    <p className="char-name">MSM</p>
                    <canvas id="SUBKP2" />
                </div>
                <div className="col-xl-4 bo-r">
                    <p className="char-name">PWID</p>
                    <canvas id="SUBKP3" />
                </div>
                <div className="col-xl-8 select-district charthide">
                    <canvas id="csotpepic" />
                </div>
            </div>
        </div>
    </Container>
)

export default SubTypologyCoverage