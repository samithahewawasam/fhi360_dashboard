import React, { Component } from 'react';
import { Container, Form, Button, Col, Nav, Row, Dropdown, Tab } from 'react-bootstrap';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import compose from 'recompose/compose';
import { getCoverage } from '../../Actions/coverage'

//STDServices tabs
import NationalLevel from './STDServices-tabs/NationalLevel'
import DistrictsLevel from './STDServices-tabs/DistrictsLevel'

//utility
import { years, quarters } from './utility'

class STDServices extends Component {

  render() {
    
    const { coverage } = this.props

    return (

      <div className="col-xl-12">

        <Tab.Container id="left-tabs-example" defaultActiveKey="1">
          <Row>
            <Col sm={3}>
              <Nav variant="pills" className="flex-column">
                <Nav.Item>
                  <Nav.Link eventKey="1">STD Services at National Level</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="2">STD Services at Districts Level</Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
            <Col sm={9}>
              <Tab.Content>
                <Tab.Pane eventKey="1">
                  <NationalLevel coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
                <Tab.Pane eventKey="2">
                  <DistrictsLevel coverage={coverage} years={years} quarters={quarters} />
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    coverage: state.coverage,
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({ getCoverage }, dispatch)

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

export default enhance(STDServices)