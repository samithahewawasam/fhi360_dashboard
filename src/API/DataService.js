const token = "eyJhbGciOiJIUzI1NiIsImN0eSI6InRleHRcL3BsYWluIn0.eyJpZCI6Imdpc1NlcnZpY2UiLCJleHAiOjE1Njc5NjEwNTkxNjJ9.gghs9a9ue0nSLcAaRA7W7hcqGG8WIhCGeIMQJmiBR-w"
var url = new URL('https://me.fpasrilanka.org/restService.htm')

const DataService = (params) => {

    params.token = token
    url.search = new URLSearchParams(params)

    return new Promise((resolve, reject) => {

        fetch(url, {
            method: 'POST',
            mode: 'no-cors',
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(res => {
                res.json()
            })
            .then(function (text) {
                resolve(text)
            })
            .catch(function (error) {
                reject(error);
            });


    })

}

export default DataService
