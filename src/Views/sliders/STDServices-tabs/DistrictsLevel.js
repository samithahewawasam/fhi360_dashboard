import React, { Component } from 'react';
import { Container, Form, Button, Navbar, Nav, Dropdown, FormControl, ListGroup } from 'react-bootstrap';
import { Bar } from 'react-chartjs-2';

const DistrictsLevel = ({ coverage, years, quarters }) => (
    <Container fluid>
        <div className="row">
            <div className="col-xl-12">
                <form>
                    <div className="row bo-b">
                        <div className="col-lg-5">
                            <div className="main-title">
                                <h3>STD Services Reported by KP in Districts</h3>
                            </div>
                        </div>
                        <div className="col-lg-7">
                            <ul className="filters">
                                <li>
                                    <div className="btn-group open">
                                        <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown2" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                            Districts
                                      </button>
                                        <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                            <a className="dropdown-item all-district-select" href="#" role="menuitem">All Districts </a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">Anuradhapura</a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">Ampara</a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">Badulla</a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">Colombo</a>
                                            <a className="dropdown-item selected-district-select" href="#" role="menuitem">Gampha</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="btn-group open">
                                        <button type="button" className="btn btn-outline btn-default dropdown-toggle" id="exampleHoverDropdown2" data-hover="dropdown" data-delay={1000} data-toggle="dropdown" aria-expanded="false">
                                            Years
                                      </button>
                                        <div className="dropdown-menu" aria-labelledby="exampleHoverDropdown1" role="menu" x-placement="bottom-start" style={{ position: 'absolute', transform: 'translate3d(0px, 36px, 0px)', top: '0px', left: '0px', willChange: 'transform' }}>
                                            <a className="dropdown-item " href="#" role="menuitem">2019 </a>
                                            <a className="dropdown-item " href="#" role="menuitem">2018</a>
                                            <a className="dropdown-item " href="#" role="menuitem">2017</a>
                                            <a className="dropdown-item " href="#" role="menuitem">2016</a>
                                            <a className="dropdown-item " href="#" role="menuitem">2015</a>
                                            <a className="dropdown-item " href="#" role="menuitem">2014</a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div className="btn-toolbar" aria-label="Toolbar with button groups" role="toolbar">
                                        <div className="btn-group" role="group">
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="icon wb-print" aria-hidden="true" /></button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> CSV</button>
                                            <button type="button" className="btn btn-icon btn-default btn-outline"><i className="wb-download" /> PDF</button>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div className="row">
            <div className="main-page all-district">
                <div className="col-xl-4 bo-r">
                    <h1>STD Services Reported by KP in All Districts</h1>
                    <div className="heading-baneer">
                        <div className="icon-img">
                            <img src="assets/examples/images/icon/25.png" alt="" />
                        </div>
                        <div className="titele">
                            <h2>2,500</h2>
                            <h3>MSM : 154</h3>
                            <h3>FSW : 245</h3>
                            <h3>BB : 504</h3>
                            <h3>PWID : 204</h3>
                            <h3>TGW : 242</h3>
                        </div>
                    </div>
                    <div className="titele2">
                    </div>
                </div>
                <div className="col-xl-8 ">
                    <canvas id="STDDLBAR" />
                </div>
            </div>
            <div className="main-page  select-district charthide">
                <div className="col-xl-4 bo-r">
                    <h1>STD Services Reported by KP in Anuradhapura District </h1>
                    <div className="heading-baneer">
                        <div className="icon-img">
                            <img src="assets/examples/images/icon/28.png" alt="" />
                        </div>
                        <div className="titele">
                            <h2>1,000</h2>
                            <h3>MSM : 154</h3>
                            <h3>FSW : 25</h3>
                            <h3>BB : 50</h3>
                            <h3>PWID : 20</h3>
                            <h3>TGW : 22</h3>
                        </div>
                    </div>
                    <div className="titele2">
                    </div>
                </div>
                <div className="col-xl-4">
                    <canvas id="STDDLPIE" />
                </div>
                <div className="col-xl-4 ">
                    <canvas id="STDDLQU" />
                </div>
            </div>
        </div>
    </Container>
)

export default DistrictsLevel