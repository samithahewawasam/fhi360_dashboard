import { call, put, takeEvery, takeLatest, all, select } from 'redux-saga/effects'
import { NotificationManager } from 'react-notifications';
import { signInSuccess, signInFail, signOutSuccess, signOutFail } from '../Actions/User'
import { SignIn } from '../API/User'
import { getCoverageData } from '../API/coverage'
import { getCoverageSuccess, getCoverageFail } from '../Actions/coverage'
import { getAlerts } from '../API/Alert'
import { getAlertsSuccess, getAlertsFail } from '../Actions/alert'

import history from '../history';

function* SignIn_(action) {

    try {

        let data = yield call(SignIn, action.login);
        yield put(signInSuccess(data))


    } catch (error) {
        NotificationManager.error('', 'Username or password incorrect')
        yield put(signInFail(error))

    }

}


export function* WatchSignIn() {
    yield takeLatest('SIGN_IN', SignIn_);
}

function* SignInSuccess(action) {

    try {

        localStorage.setItem('access_token', action.data.access_token)
        localStorage.setItem('refresh_token', action.data.refresh_token)
        localStorage.setItem('isLogged', true)
        history.push("/")
        NotificationManager.success('', 'Successfully logged');

    } catch (error) {

        localStorage.removeItem('access_token')
        localStorage.removeItem('refresh_token')
        localStorage.removeItem('isLogged')
        NotificationManager.error('', 'Username or password incorrect')

    }

}

export function* WatchSignInSuccess() {
    yield takeLatest('SIGN_IN_SUCCESS', SignInSuccess);
}

function* SignOut(action) {

    try {

        yield put(signOutSuccess())

    } catch (error) {

        yield put(signOutFail())
        history.push("/")

    }

}

export function* WatchSignOut() {
    yield takeLatest('SIGN_OUT', SignOut);
}

function* SignOutSuccess(action) {

    try {

        localStorage.removeItem('token')
        localStorage.removeItem('isLogged')
        history.push("/login")

    } catch (error) {

        history.push("/")

    }

}

export function* WatchSignOutSuccess() {
    yield takeLatest('SIGN_OUT_SUCCESS', SignOutSuccess);
}

//coverage start 
function* coverage(action) {

    try {
        
        let data = yield call(getCoverageData, action.data);
        
        yield put(getCoverageSuccess(data))


    } catch (error) {
        yield put(getCoverageFail(error))

    }

}

export function* WatchCoverage() {
    yield takeLatest('GET_COVERAGE', coverage);
}

//coverage end 

//coverage start 
function* alerts(action) {

    try {

        let data = yield call(getAlerts, action.data);
        
        yield put(getAlertsSuccess(data))


    } catch (error) {
        yield put(getAlertsFail(error))

    }

}

export function* WatchAlerts() {
    yield takeLatest('GET_ALERTS', alerts);
}

//coverage end 

export default function* rootSaga() {
    yield all([
        WatchSignIn(),
        WatchSignInSuccess(),
        WatchSignOut(),
        WatchSignOutSuccess(),
        WatchCoverage(),
        WatchAlerts()
    ])
}