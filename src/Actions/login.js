export const login = data => ({ type: 'LOGIN' , data})
export const login_success = data => ({ type: "LOGIN_SUCCESS", data })
export const login_fail = error => ({ type: "LOGIN_FAIL", error })