import React from "react";
import { Switch, Route, Link } from "react-router-dom";
import Home from './Views/home';
import Login from './Views/Login'
import PrivateRoute from './Components/Auth/PrivateRoute'
import 'react-notifications/lib/notifications.css';
const AppRouter = () => (

  <Switch>
    <PrivateRoute path='/' exact component={Home} />
    <Route path='/login' component={Login} />
    <Route path='/register' component={Home} />
    <PrivateRoute path='/dashboard' component={Home} />
  </Switch>

)

export default AppRouter;