import login from './login';
import coverage from './coverage';
import alerts from './alerts';

import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

const rootReducer = combineReducers({
    login,
    coverage,
    alerts,
    form: formReducer
})

export default rootReducer