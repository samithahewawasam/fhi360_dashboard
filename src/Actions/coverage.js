export const getCoverage = data => ({ type: 'GET_COVERAGE' , data})
export const getCoverageSuccess = data => ({ type: "GET_COVERAGE_SUCCESS", data })
export const getCoverageFail= error => ({ type: "GET_COVERAGE_FAIL", error })
export const getCoverageSample = data => ({ type: "GET_COVERAGE", data })